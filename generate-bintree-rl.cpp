#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;



inline string left(int i) {
  stringstream s;
  s << "left" << i;
  string result;
  s >> result;
  return result;
}
inline string right(int i) {
  stringstream s;
  s << "right" << i;
  string result;
  s >> result;
  return result;
}

int main() {
  int temp;
  cerr << "A model of a tree with (2^n)-1 nodes will be created." << endl;
  cerr << "Enter n: ";
  cin >> temp;
  const int BOUND = temp;
  int n = 2;
  while(temp>1) {
    n *= 2;
    --temp;
  }
  const int ELEMENTS_IN_TREE = n-1;
  cout << "dtmc" << endl << endl;
  cout << "const int ELEMENTS=" << ELEMENTS_IN_TREE << ";" << endl;
  cout << "const int BOUND=" << BOUND << ";" << endl;

  cout << "formula bintree_invariant = (true) ";
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "& (" << right(elem) << " >= " << elem << ") & (";
    cout << left(elem) << " <= " << elem << ") ";  
  }
  cout << ";" << endl;
  cout << endl << endl << "module bintree" << endl;
  for (int i = 1; i <= BOUND; ++i) {
    cout << right(i) << ": [1..BOUND] init " << i << ";" << endl;
  }
  for (int i = 1; i <= BOUND; ++i) {
    cout << left(i) << ": [1..BOUND] init " << i << ";" << endl;
  }

  cout << "root : [0..BOUND] init 0;" << endl;
  cout << "element:[0..BOUND] init 0;" << endl; 
  cout << "flag_add:bool init false;" << endl;
  cout << "flag_delete:bool init false;" << endl;
  cout << "actually_add:bool init false;" << endl;
  cout << "hit_bug:bool init false;" << endl;
  cout << "current:[0..BOUND] init 0;" << endl;
  cout << "replacement:[0..BOUND] init 0;" << endl;
  cout << "total:[0..1] init 0;" << endl;


  cout << "[bug_sink] hit_bug -> (hit_bug'=true);" << endl;
  cout << "[tick] !hit_bug & bintree_invariant & !flag_delete & !flag_add -> "
          "0.5:(flag_delete'=true)&(current'=0)&(total'=1)&(element'=0)&(replacement'=0) + "
          "0.5:(flag_add'=true)&(current'=0)&(total'=1)&(element'=0)&(replacement'=0);" << endl;
  cout << "[choose_element] !hit_bug & element=0 & replacement=0 & current=0 & "
       << "(flag_add | flag_delete) -> ";
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "(1/BOUND):(element'=" << elem <<") ";
    cout << ((elem == BOUND) ? ";" : " + "); 
  }
  cout << endl;
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "[trivial_add] !hit_bug & flag_add & !actually_add & root=element & element!=0 "
         << "-> (element'=0)&(flag_add'=false); " << endl;
    for (int other_elem  = 1; other_elem < elem; ++other_elem) {
      cout << "[trivial_add] !hit_bug & flag_add & !actually_add & root!=element & element="
           << elem << " & " << right(other_elem) << "=element "
           << "-> (element'=0)&(flag_add'=false); " << endl;
    }
    for (int other_elem  = elem+1; other_elem <= BOUND; ++other_elem) {
      cout << "[trivial_add] !hit_bug & flag_add & !actually_add & root!=element & element="
           << elem << " & " << left(other_elem) << "=element "
           << "-> (element'=0)&(flag_add'=false); " << endl;
    }
  }
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "[start_nontrivial_add] !hit_bug & flag_add & !actually_add & root!=element & element="
         << elem;
    for (int other_elem  = 1; other_elem < elem; ++other_elem) {
      cout << " & " << right(other_elem) << "!=element ";
    }
    for (int other_elem  = elem+1; other_elem <= BOUND; ++other_elem) {
      cout << " & " << left(other_elem) << "!=element ";
    }
    cout << "-> (actually_add'=true); " << endl;
  }

  cout << "[add_root] !hit_bug & actually_add & root=0 "
       << " -> (root'=element)&(flag_add'=false)&(actually_add'=false);" << endl;
  cout << "[start_add_traversal] !hit_bug & actually_add & root!=0 & current=0"
       << " -> (current'=root);" << endl;

  for (int i =1; i < BOUND ; ++i ) {
    cout << "[add_right] !hit_bug & actually_add & current=" << i
         << " & flag_add & root != 0 & element > 0 & " << right(i) << "=" << i
         << " & current < element "
         << " -> (" << right(i)
         << "'=element)&(flag_add'=false)&(actually_add'=false);" << endl; 
  } 
  for (int i =2; i <= BOUND ; ++i ) {
    cout << "[add_left] !hit_bug & actually_add & current=" << i
         << " & flag_add & root != 0 & element > 0 & " << left(i) << "=" << i
         << " & current > element "
         << " -> (" << left(i)
         << "'=element)&(flag_add'=false)&(actually_add'=false);" << endl; 
  } 

  for (int i =1; i < BOUND ; ++i ) {
    cout << "[traverse_right_add] !hit_bug & actually_add & current=" << i
         << " & flag_add & root != 0 & " << right(i) << "!=" << i
         << " & current < element "
         << " -> (current'=" << right(i) << ");" << endl; 
  } 
  for (int i =2; i <= BOUND ; ++i ) {
    cout << "[traverse_left_add] !hit_bug & actually_add & current=" << i
         << " & flag_add & root != 0 & " << left(i) << "!=" << i
         << " & current > element "
         << " -> (current'=" << left(i) << ");" << endl; 
  } 
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "[trivial_delete] replacement=0 &!hit_bug & bintree_invariant & flag_delete & root!=element & element="
         << elem;
    for (int other_elem  = 1; other_elem < elem; ++other_elem) {
      cout << " & " << right(other_elem) << "!=element ";
    }
    for (int other_elem  = elem+1; other_elem <= BOUND; ++other_elem) {
      cout << " & " << left(other_elem) << "!=element ";
    }
    cout << "-> (flag_delete'=false); " << endl;
  }
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "[correct_delete] replacement=0 & !hit_bug & bintree_invariant"
         << " & element=" << elem 
         << " & root=element & flag_delete ";
    if (elem!=BOUND) cout << " & " << right(elem) << "=" << elem;
    if (elem!=1) cout << " & " << left(elem) << "=" << elem;
    cout << " -> (root'=0)&"
         << "(flag_delete'=false);" << endl;
    for (int other_elem =1; other_elem < elem ; ++other_elem ) {
      cout << "[correct_delete] replacement=0 & !hit_bug & bintree_invariant "
           << " & element=" << elem 
           << " & root!=element & flag_delete & ";
      if (elem!=BOUND) cout << right(elem) << "=" << elem << " & ";
      if (elem!=1) cout << left(elem) << "=" << elem << " & ";
      cout << right(other_elem) << "=" << elem << " "
           << " -> (" << right(other_elem) << "'=" << other_elem << ")&"
           << "(flag_delete'=false);" << endl;
    }
    for (int other_elem =elem+1; other_elem <= BOUND ; ++other_elem ) {
      cout << "[correct_delete] !hit_bug & bintree_invariant & current=0 & replacement=0 "
           << " & element=" << elem 
           << " & flag_delete & ";
      if (elem!=BOUND) cout << right(elem) << "=" << elem << " & ";
      if (elem!=1) cout << left(elem) << "=" << elem << " & ";
      cout << left(other_elem) << "=" << elem << " "
           << " -> (" << left(other_elem) << "'=" << other_elem << ")&"
           << "(flag_delete'=false);" << endl;
    }
  }
  for (int elem = 1; elem <= BOUND; ++elem) {
    for (int go_left_on_1 = 0; go_left_on_1 < 2; ++go_left_on_1) {
      cout << "[bugged_delete_start]";
      cout << " !hit_bug & bintree_invariant & current=0 & replacement=0  ";
      cout << " & element=" << elem; 
      if (go_left_on_1 == 0) {
        cout << " & " << right(elem) << "!= element "; 
      } else {
        cout << " & " << right(elem) << "=" << elem << " & "
             << left(elem) << "!=" << elem;
      }
        cout << " & flag_delete ";
      cout << " -> (replacement'=";
      if (go_left_on_1 == 0) {
        cout << right(elem) << ")";
      } else {
        cout << left(elem) << ")";
      }
      cout << "&(current'=" << elem << ");" << endl;
    }
  }

  for (int cur = 1; cur <= BOUND; ++cur) {
    for (int rep = 1; rep <= BOUND; ++rep) {
      if (cur == rep) continue;
      for (int go_left_on_1 = 0; go_left_on_1 < 2; ++go_left_on_1) {
        if ((rep == 1 && go_left_on_1 == 1) || (rep == BOUND && go_left_on_1 == 0)) continue; 
        if (go_left_on_1 == 0) {
          cout << "[bugged_delete_traverse_right]";
        } else {
          cout << "[bugged_delete_traverse_left]";
        }
        cout << " bintree_invariant & !hit_bug & element!=0 & current=" << cur;
        cout << " & replacement=" << rep << " & !hit_bug ";
        cout << " & element!=0 " 
             << " & flag_delete & ";
        if (go_left_on_1 == 0) {
          cout << right(rep) << "!=" << rep;
        } else {
          cout << right(rep) << "=" << rep
               << " & " << left(rep) << "!=" << rep;
        }
        cout << " -> (replacement'=";
        if (go_left_on_1 == 0) {
          cout << right(rep) << ")";
        } else {
          cout << left(rep) << ")";
        }
        cout << "&(current'=replacement);" << endl;
      }
    }
  }

  for (int cur = 1; cur <= BOUND; ++cur) {
    for (int rep = 1; rep <= BOUND; ++rep) {
      for (int elem = 1; elem <= BOUND; ++elem) {
        for (int father = 0; father <= BOUND; ++father) {
          int elements[4] = {cur, rep, elem, father};
          bool repeated = false;
          for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
              if(i==j) continue;
              repeated = repeated || elements[i] == elements[j];
            }
          }
          if (repeated) continue;
          for (int go_left_on_1_current = 0; go_left_on_1_current < 2; ++go_left_on_1_current) {
            for (int go_left_on_1_father = 0; go_left_on_1_father < 2; ++go_left_on_1_father) {
              for (int element_has_left_on_1 = 0; element_has_left_on_1 < 2; ++element_has_left_on_1) {
                for (int element_has_right_on_1 = 0; element_has_right_on_1 < 2; ++element_has_right_on_1) {
                  if ( (cur == 1 && go_left_on_1_current == 1) || (cur == BOUND && go_left_on_1_current == 0)) continue;
                  if ( (father == 1 && go_left_on_1_father == 1) || (father == BOUND && go_left_on_1_father == 0)) continue;
                  cout << "[replace_element] bintree_invariant & !hit_bug & ";
                  if (father!=0 && go_left_on_1_father == 0) {
                    cout << right(father) << "=element & ";
                  }
                  if (father!=0 && go_left_on_1_father == 1) {
                    cout << left(father) << "=element & ";
                  }
                  if (cur != BOUND && go_left_on_1_current == 0) {
                    cout << right(cur) << "=replacement & ";
                  }
                  if (cur != 1 && go_left_on_1_current == 1) {
                    cout << left(cur) << "=replacement & ";
                  }
                  if (elem!=1) {
                    if (element_has_left_on_1==1) {
                      cout << left(elem) << "!=element & ";
                    } else {
                      cout << left(elem) << "=element & ";
                    }
                  }
                  if (elem!=BOUND) {
                    if (element_has_right_on_1==1) {
                      cout << right(elem) << "!=element & ";
                    } else {
                      cout << right(elem) << "=element & ";
                    }
                  }
                  if (father == 0) {
                    cout << "root=element & ";
                  }
                  cout << " element=" << elem << " & current=" << cur;
                  cout << " & replacement=" << rep << " & !hit_bug ";
                  cout << " & element!=0 " 
                       << " & flag_delete";
                  if (rep!=BOUND) {
                    cout << " & " << right(rep) << "=" << rep;
                  }
                  if (rep!=1) {
                    cout << " & " << left(rep) << "=" << rep;
                  }
                  cout << " -> (element'=0)&(replacement'=0)&(current'=0)&(flag_delete'=false"; 
                  if (elem != 1) {
                    cout << ")&(" << left(elem) << "'="<< elem;
                  }
                  if (elem != BOUND) {
                    cout << ")&(" << right(elem) << "'=" << elem;
                  }
                  if (father!=0 && go_left_on_1_father==0) {
                    cout << ")&(" << right(father) << "'=replacement";
                  }
                  if (father!=0 && go_left_on_1_father==1) {
                    cout << ")&(" << left(father) << "'=replacement";
                  }
                  if (father==0) {
                    cout << ")&(root'=replacement";
                  }
                  if (cur!=BOUND && go_left_on_1_current == 0) {
                    cout << ")&(" << right(cur) << "'=" << cur;
                  }
                  if (cur!=1 && go_left_on_1_current ==1) {
                    cout << ")&(" << left(cur) << "'=" << cur;
                  }
                  if (elem != 1) {
                    if (element_has_left_on_1 == 1) {
                      cout << ")&(" << left(rep) << "'=" << left(elem);
                    } else {
                      cout << ")&(" << left(rep) << "'=" << rep;
                    }
                  }
                  if (elem != BOUND) {
                    if (elem != BOUND && element_has_right_on_1 == 1) {
                      cout << ")&(" << right(rep) << "'=" << right(elem);
                    } else {
                      cout << ")&(" << right(rep) << "'=" << rep;
                    }
                  }
                  cout << ");" << endl;
                }
              }
            }
          }
        }
      }
    }
  }
  
  // Same thing, now cur and elem are equal.
  for (int rep = 1; rep <= BOUND; ++rep) {
    for (int elem = 1; elem <= BOUND; ++elem) {
      for (int father = 0; father <= BOUND; ++father) {
        int elements[4] = {rep, elem, father};
        bool repeated = false;
        for (int i = 0; i < 3; ++i) {
          for (int j = 0; j < 3; ++j) {
            if(i==j) continue;
            repeated = repeated || elements[i] == elements[j];
          }
        }
        if (repeated) continue;
        for (int go_left_on_1_current = 0; go_left_on_1_current < 2; ++go_left_on_1_current) {
          for (int go_left_on_1_father = 0; go_left_on_1_father < 2; ++go_left_on_1_father) {
            for (int element_has_left_on_1 = 0; element_has_left_on_1 < 2; ++element_has_left_on_1) {
              for (int element_has_right_on_1 = 0; element_has_right_on_1 < 2; ++element_has_right_on_1) {
                if ( (elem == 1 && go_left_on_1_current == 1) || (elem == BOUND && go_left_on_1_current == 0)) continue;
                if ( (father == 1 && go_left_on_1_father == 1) || (father == BOUND && go_left_on_1_father == 0)) continue;
                cout << "[replace_element] bintree_invariant & !hit_bug & ";
                if (father!=0 && go_left_on_1_father == 0) {
                  cout << right(father) << "=element & ";
                }
                if (father!=0 && go_left_on_1_father == 1) {
                  cout << left(father) << "=element & ";
                }
                if (elem != BOUND && go_left_on_1_current == 0) {
                  cout << right(elem) << "=replacement & ";
                }
                if (elem != 1 && go_left_on_1_current == 1) {
                  cout << left(elem) << "=replacement & ";
                }
                if (elem!=1) {
                  if (element_has_left_on_1==1) {
                    cout << left(elem) << "!=element & ";
                  } else {
                    cout << left(elem) << "=element & ";
                  }
                }
                if (elem!=BOUND) {
                  if (element_has_right_on_1==1) {
                    cout << right(elem) << "!=element & ";
                  } else {
                    cout << right(elem) << "=element & ";
                  }
                }
                if (father == 0) {
                  cout << "root=element & ";
                }
                cout << " element=" << elem << " & current=element";
                cout << " & replacement=" << rep << " & !hit_bug ";
                cout << " & element!=0 " 
                     << " & flag_delete";
                if (rep!=BOUND) {
                  cout << " & " << right(rep) << "=" << rep;
                }
                if (rep!=1) {
                  cout << " & " << left(rep) << "=" << rep;
                }
                cout << " -> (element'=0)&(replacement'=0)&(current'=0)&(flag_delete'=false"; 
                if (elem != 1) {
                  cout << ")&(" << left(elem) << "'="<< elem;
                }
                if (elem != BOUND) {
                  cout << ")&(" << right(elem) << "'=" << elem;
                }
                if (father!=0 && go_left_on_1_father==0) {
                  cout << ")&(" << right(father) << "'=replacement";
                }
                if (father!=0 && go_left_on_1_father==1) {
                  cout << ")&(" << left(father) << "'=replacement";
                }
                if (father==0) {
                  cout << ")&(root'=replacement";
                }
                if (elem != 1) {
                  if (element_has_left_on_1 == 1) {
                    cout << ")&(" << left(rep) << "'=" << left(elem);
                  } else {
                    cout << ")&(" << left(rep) << "'=" << rep;
                  }
                }
                if (elem != BOUND) {
                  if (elem != BOUND && element_has_right_on_1 == 1) {
                    cout << ")&(" << right(rep) << "'=" << right(elem);
                  } else {
                    cout << ")&(" << right(rep) << "'=" << rep;
                  }
                }
                cout << ");" << endl;
              }
            }
          }
        }
      }
    }
  }
  cout << "[check_bintree_invariant] !hit_bug & !bintree_invariant -> (hit_bug'=true); " << endl;
  cout << "[check_bintree_invariant] !hit_bug & !bintree_invariant -> (hit_bug'=true); " << endl;

  cout << "endmodule" << endl << endl;
  cout << "rewards" << endl;
  cout << "[tick] true : 1;" << endl;
  cout << "endrewards" << endl;
  return 0;
}
