#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;



inline string left(int i) {
  stringstream s;
  s << "left" << i;
  string result;
  s >> result;
  return result;
}
inline string right(int i) {
  stringstream s;
  s << "right" << i;
  string result;
  s >> result;
  return result;
}

int main() {
  int temp;
  cerr << "A model of a tree with (2^n)-1 nodes will be created." << endl;
  cerr << "Enter n: ";
  cin >> temp;
  const int BOUND = temp;
  int n = 2;
  while(temp>1) {
    n *= 2;
    --temp;
  }
  const int ELEMENTS_IN_TREE = n-1;
  cout << "dtmc" << endl << endl;
  cout << "const int ELEMENTS=" << ELEMENTS_IN_TREE << ";" << endl;
  cout << "const int BOUND=" << BOUND << ";" << endl;

  cout << "formula bintree_invariant = (true) ";
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "& (" << right(elem) << "=0 | " << right(elem) << " > " << elem << ") & (";
    cout << left(elem) << " < " << elem << ") ";  
  }
  cout << ";" << endl;
  cout << endl << endl << "module bintree" << endl;
  for (int i = 1; i <= BOUND; ++i) {
    cout << right(i) << ": [0..BOUND] init 0;" << endl;
  }
  for (int i = 1; i <= BOUND; ++i) {
    cout << left(i) << ": [0.." << i << "] init 0;" << endl;
  }

  cout << "root : [0..BOUND] init 0;" << endl;
  cout << "element:[0..BOUND] init 0;" << endl; 
  cout << "flag_add:bool init false;" << endl;
  cout << "flag_delete:bool init false;" << endl;
  cout << "hit_bug:bool init false;" << endl;
  cout << "current:[0..BOUND] init 0;" << endl;
  cout << "parent:[0..BOUND] init 0;" << endl;
  cout << "bigson:[0..BOUND] init 0;" << endl;
  cout << "total:[0..1] init 0;" << endl;
  cout << "branch:bool init false;" << endl;
  
  cout << "line:[0..9] init 0;" << endl;


  cout << "[bug_sink] hit_bug -> (hit_bug'=true);" << endl;
  cout << "[tick] !hit_bug & bintree_invariant & !flag_delete & !flag_add -> "
          "0.5:(flag_delete'=true)&(current'=0)&(total'=1)&(element'=0) + "
          "0.5:(flag_add'=true)&(current'=0)&(total'=1)&(element'=0);" << endl;
  cout << "[choose_element] !hit_bug & element=0 & current=0 & "
       << "(flag_add | flag_delete) -> ";
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "(1/BOUND):(element'=" << elem <<") ";
    cout << ((elem == BOUND) ? ";" : " + "); 
  }
  cout << endl;
  
  cout << "[pre_loop] flag_delete & line=0 -> (current'=root)&(parent'=0)&(branch'=true)&(line'=1);" << endl;
  cout << "[checkBoolLoop1Pos] flag_delete & line=1 & (current!=0) -> (line'=2);" << endl;
  cout << "[checkBoolLoop1Neg] flag_delete & line=1 & !(current!=0) -> (line'=9);" << endl;
  cout << "[checkIf1Pos] flag_delete & line=2 & (current=element) -> (line'=3)&(bigson'=current);" << endl;
  cout << "[checkIf1Neg] flag_delete & line=2 & !(current=element) -> (line'=8)&(bigson'=current);" << endl;
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkBoolLoop2Pos] flag_delete & line=3 & bigson=" << bigson << " & (" 
         << right(bigson) << "!=0 | " << left(bigson) << "!=0)"
         << " -> (line'=4)&(parent'=bigson);" << endl;
  }
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkBoolLoop2Neg] flag_delete & line=3 & bigson=" << bigson << " & !(" 
         << right(bigson) << "!=0 | " << left(bigson) << "!=0)"
         << " -> (line'=5);" << endl;
  }
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkIf2Pos] flag_delete & line=4 & bigson=" << bigson << " & ("
         << right(bigson) << "!= 0) -> (bigson'=" << right(bigson) << ")&(branch'=false)&(line'=3);" << endl; 
  }
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkIf2Neg] flag_delete & line=4 & bigson=" << bigson << " & !("
         << right(bigson) << "!= 0) -> (bigson'=" << left(bigson) << ")&(branch'=true)&(line'=3);" << endl; 
  }
  
  cout << "[checkIf3Pos] flag_delete & line=5 & (parent!=0) -> (line'=6);" << endl;
  cout << "[checkIf3Neg] flag_delete & line=5 & !(parent!=0) -> (line'=7);" << endl;

  for (int parent = 1; parent <= BOUND; ++parent) {
    cout << "[checkIf4Pos] flag_delete & line=6 & (branch) & parent=" << parent << " -> (line'=7)&(" << left(parent) << "'=0);" << endl;
  }
  for (int parent = 1; parent <= BOUND; ++parent) {
    cout << "[checkIf4Neg] flag_delete & line=6 & !(branch) & parent=" << parent << "-> (line'=7)&(" << right(parent) << "'=0);" << endl;
  }

  // TODO check this 1000 times.
  //cout << "[checkIf5Pos] flag_delete & line=7 & (bigson!=current) -> (current'=bigson)&(line'=9);" << endl;

  for (int element = 1; element <= BOUND; ++element) {
    for (int bigson = 1; bigson <= BOUND; ++bigson) {
      if (element == bigson) continue;
      cout << "[checkIf5Pos] flag_delete & line=7 & (bigson!=current) & root=element & element=" << element
           << " & bigson=" << bigson << " -> (root'=bigson)&(line'=9)&("
           << right(element) << "'=0)&(" << left(element) << "'=0);" << endl;
      for (int father = 1; father <= BOUND; ++father) {
        if (father == element || father == bigson) continue;
        cout << "[checkIf5Pos] flag_delete & line=7 & (bigson!=current) & " << right(father)
             << "=element & element=" << element
             << " & bigson=" << bigson << " -> (" << right(father) << "'=bigson)&(line'=9)&("
             << right(element) << "'=0)&(" << left(element) << "'=0)&("
             << right(bigson) << "'=" << right(element) << ")&("
             << left(bigson) << "'=" << left(element) << ");" << endl;
      }
      for (int father = 1; father <= BOUND; ++father) {
        if (father == element || father == bigson) continue;
        cout << "[checkIf5Pos] flag_delete & line=7 & (bigson!=current) & " << left(father)
             << "=element & element=" << element
             << " & bigson=" << bigson << " -> (" << left(father) << "'=bigson)&(line'=9)&("
             << right(element) << "'=0)&(" << left(element) << "'=0)&("
             << right(bigson) << "'=" << right(element) << ")&("
             << left(bigson) << "'=" << left(element) << ");" << endl;
      }
    }
  }


  cout << "[checkIf5Neg] flag_delete & line=7 & !(bigson!=current) -> (line'=9);" << endl;

  for (int current = 1; current <= BOUND; ++current) {
    cout << "[checkIf6Pos] flag_delete & line=8 & current=" << current
         << " & (current > element) -> (line'=1)&(current'=" << left(current) << ")&(branch'=true);" << endl;
  }
  for (int current = 1; current <= BOUND; ++current) {
    cout << "[checkIf6Neg] flag_delete & line=8 & current=" << current
         << " & !(current > element) -> (line'=1)&(current'=" << right(current) << ")&(branch'=false);" << endl;
  }

  cout << "[explicit_invariant_check] flag_delete & line=9 & !hit_bug & !bintree_invariant -> (hit_bug'=true);" << endl;
  cout << "[explicit_invariant_check] flag_delete & line=9 & !hit_bug & bintree_invariant -> (line'=0)&(flag_delete'=false);" << endl;


  cout << "[pre_add] flag_add & line=0 -> (current'=root)&(line'=1);" << endl;
  cout << "[checkIf1Pos] flag_add & line=1 & (root=0) -> (root'=element)&(line'=0)&(flag_add'=false);" << endl; 
  cout << "[checkIf1Neg] flag_add & line=1 & !(root=0) -> (line'=2);" << endl; 
  
  cout << "[checkBoolLoop1Pos] flag_add & line=2 & (current!=element) -> (line'=3);" << endl;


  cout << "[checkIf2Pos] flag_add & line=3 & (element<current) -> (line'=4);" << endl;
  cout << "[checkIf2Neg] flag_add & line=3 & !(element<current) -> (line'=5);" << endl;

  for (int current=1;current<=BOUND; ++current) {
    cout << "[checkIf3Pos] flag_add & line=4 & current=" << current << " & (" << left(current) << "=0) -> (line'=2)&(" << left(current) << "'=element);" << endl;
  }
  for (int current=1;current<=BOUND; ++current) {
    cout << "[checkIf3Neg] flag_add & line=4 & current=" << current << " & !(" << left(current) << "=0) -> (line'=2)&(current'=" << left(current) << ");" << endl;
  }

  for (int current=1;current<=BOUND; ++current) {
    cout << "[checkIf4Pos] flag_add & line=5 & current=" << current << " & (" << right(current) << "=0) -> (line'=2)&(" << right(current) << "'=element);" << endl;
  }
  for (int current=1;current<=BOUND; ++current) {
    cout << "[checkIf4Neg] flag_add & line=5 & current=" << current << " & !(" << right(current) << "=0) -> (line'=2)&(current'=" << right(current) << ");" << endl;
  }
  

//  cout << "[check_bintree_invariant] !hit_bug & !bintree_invariant -> (hit_bug'=true); " << endl;
//  cout << "[check_bintree_invariant] !hit_bug & !bintree_invariant -> (hit_bug'=true); " << endl;



  cout << "endmodule" << endl << endl;
  cout << "rewards" << endl;
  cout << "[tick] true : 1;" << endl;
  cout << "endrewards" << endl;
  return 0;
}
