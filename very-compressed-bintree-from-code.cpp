#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;



inline string left(int i) {
  stringstream s;
  s << "left" << i;
  return s.str();
}
inline string right(int i) {
  stringstream s;
  s << "right" << i;
  return s.str();
}

const string bt_invariant(int lower, int upper, int key, int BOUND, int former_BOUND) {
  if (BOUND==0) return " true ";
  if (!(lower < key & key < upper)) return " false ";
  stringstream s;
  s << "((" << right(key) << "=BOUND+1 ";
  for (int r = key+1; r < upper; ++r) {
    s << endl;
    for (int i = BOUND; i < former_BOUND; ++i) {
      s << "  ";
    }
    s << "| (" << right(key) << "=" << r << " & " << bt_invariant(key, upper, r, BOUND-1, former_BOUND) << ")";
  }
  s << ") & (" << left(key) << "=0 ";
  for (int l = lower+1; l < key; ++l) {
    s << endl;
    for (int i = BOUND; i < former_BOUND; ++i) {
      s << "  ";
    }
    s << "| (" << left(key) << "=" << l << " & " << bt_invariant(lower, key, l, BOUND-1, former_BOUND) << ")";
  }
  s << "))";
  return s.str();
}
const string wrapper_bt_invariant(int BOUND) {
  // Assume BOUND != 0
  stringstream s;
  s << "(root=0 ";
  for (int root = 1; root <= BOUND; ++root) {
    s << " | (root=" << root << " & " << bt_invariant(0,BOUND+1, root, BOUND, BOUND) << ") "; 
  } 
  s << ")";
  return s.str();
}

int main() {
  int temp;
  cerr << "A model of a tree with (2^n)-1 nodes will be created." << endl;
  cerr << "Enter n: ";
  cin >> temp;
  const int BOUND = temp;
  int n = 2;
  while(temp>1) {
    n *= 2;
    --temp;
  }
  const int ELEMENTS_IN_TREE = n-1;
  cout << "dtmc" << endl << endl;
  cout << "const int BOUND=" << BOUND << ";" << endl;

  cout << "formula bintree_invariant = ";
  cout << wrapper_bt_invariant(BOUND);
  cout << ";" << endl;
  cout << endl << endl << "module bintree" << endl;
  for (int i = 1; i <= BOUND; ++i) {
    cout << right(i) << ": [" << (i == BOUND ? i : i + 1) <<"..BOUND+1] init BOUND+1;" << endl;
  }
  for (int i = 1; i <= BOUND; ++i) {
    cout << left(i) << ": [0.." << (i == 1 ? i : i-1) << "] init 0;" << endl;
  }

  cout << "root : [0..BOUND] init 0;" << endl;
  cout << "element:[0..BOUND] init 0;" << endl; 
  cout << "flag_add:bool init false;" << endl;
  cout << "flag_delete:bool init false;" << endl;
  cout << "hit_bug:bool init false;" << endl;
//  cout << "current:[0..BOUND] init 0;" << endl;
//  cout << "parent:[0..BOUND] init 0;" << endl;
  cout << "bigson:[0..BOUND] init 0;" << endl;
  cout << "total:[0..6] init 0;" << endl;
  cout << "branch:bool init false;" << endl;
  cout << "debug:bool init false;" << endl;
  
  cout << "line:[0..9] init 0;" << endl;

  cout << "[done] !flag_delete & !flag_add & !hit_bug & total=5 -> (total'=6);" << endl;
  cout << "[done] !flag_delete & !flag_add & !hit_bug & total=6 -> (total'=6);" << endl;
  cout << "[bug_sink] hit_bug -> (hit_bug'=true);" << endl;
  cout << "[tick] !hit_bug & total<5 & line=0 & bintree_invariant & !flag_delete & !flag_add -> "
          "0.5:(flag_delete'=true)&(total'=total+1)&(element'=0) + "
          "0.5:(flag_add'=true)&(total'=total+1)&(element'=0);" << endl;
  cout << "[choose_element] !hit_bug & element=0 & "
       << "(flag_add | flag_delete) -> ";
  for (int elem = 1; elem <= BOUND; ++elem) {
    cout << "(1/BOUND):(element'=" << elem <<") ";
    cout << ((elem == BOUND) ? ";" : " + "); 
  }
  cout << endl;
  
  cout << "[pre_loop] bintree_invariant & element!=0 & flag_delete & line=0 -> (branch'=true)&(line'=1);" << endl;
  for (int father = 1; father <= BOUND; ++father) {
    cout << "[jumpLine3] bintree_invariant &  flag_delete & line=1 & " << right(father) << "=element -> (line'=3)&(branch'=false)&(bigson'=element);" << endl;
    cout << "[jumpLine3] bintree_invariant &  flag_delete & line=1 & " << left(father) << "=element -> (line'=3)&(branch'=true)&(bigson'=element);" << endl;
  }
  cout << "[jumpLine3] bintree_invariant & flag_delete & line=1 & root=element -> (line'=3)&(bigson'=element);" << endl;
  
  cout << "[jumpAway] bintree_invariant & flag_delete & line=1 & root!=element ";
  for (int notFather = 1; notFather <= BOUND; ++notFather) {
    cout << " & " << right(notFather) << "!=element & "
         << left(notFather) << "!=element ";
  }
  cout << " -> (line'=9); " << endl;


//  cout << "[checkBoolLoop1Pos] flag_delete & line=1 & (current!=0) -> (line'=2);" << endl;
//  cout << "[checkBoolLoop1Neg] flag_delete & line=1 & !(current!=0) -> (line'=9);" << endl;
//  cout << "[checkIf1Pos] flag_delete & line=2 & (current=element) -> (line'=3)&(bigson'=current);" << endl;
//  
//  cout << "[checkIf1Neg] flag_delete & line=2 & !(current=element) -> (line'=8)&(bigson'=current);" << endl;
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkBoolLoop2Pos] bintree_invariant & flag_delete & line=3 & bigson=" << bigson << " & (" 
         << right(bigson) << "!=BOUND+1 | " << left(bigson) << "!=0)"
         << " -> (line'=4);" << endl;
  }
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkBoolLoop2Neg] bintree_invariant & flag_delete & line=3 & bigson=" << bigson << " & !(" 
         << right(bigson) << "!=BOUND+1 | " << left(bigson) << "!=0)"
         << " -> (line'=5);" << endl;
  }
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkIf2Pos] bintree_invariant & flag_delete & line=4 & bigson=" << bigson << " & ("
         << right(bigson) << "!= BOUND+1) -> (bigson'=" << right(bigson) << ")&(branch'=false)&(line'=3);" << endl; 
  }
  for (int bigson = 1; bigson <= BOUND; ++bigson) {
    cout << "[checkIf2Neg] bintree_invariant & flag_delete & line=4 & bigson=" << bigson << " & !("
         << right(bigson) << "!= BOUND+1) -> (bigson'=" << left(bigson) << ")&(branch'=true)&(line'=3);" << endl; 
  }
  
  for (int parent = 1; parent <= BOUND; ++parent) {
    cout << "[checkIf3PosIf4Pos] bintree_invariant & flag_delete & line=5 & " << left(parent) << "=bigson & branch -> (line'=7)&(" << left(parent) << "'=0);" << endl;
    cout << "[checkIf3PosIf4Neg] bintree_invariant & flag_delete & line=5 & " << right(parent) << "=bigson & !branch -> (line'=7)&(" << right(parent) << "'=BOUND+1);" << endl;
  }
  cout << "[checkIf3Neg] bintree_invariant & flag_delete & line=5 ";
  for (int notParent = 1; notParent <= BOUND; ++notParent) {
    cout << " & " << left(notParent) << "!=bigson & " << right(notParent) << "!=bigson";
  }
  cout << " -> (line'=7);" << endl;

  // TODO check this 1000 times.
  //cout << "[checkIf5Pos] flag_delete & line=7 & (bigson!=current) -> (current'=bigson)&(line'=9);" << endl;

  for (int element = 1; element <= BOUND; ++element) {
    for (int bigson = 1; bigson <= BOUND; ++bigson) {
      if (element == bigson) continue;
      cout << "[checkIf5Pos] bintree_invariant & flag_delete & line=7 & (bigson!=element) & root=element & element=" << element
           << " & bigson=" << bigson << " & (bigson > " << left(element) << " & (bigson < " << right(element) << ")) -> (root'=bigson)&(line'=9)&("
           << right(element) << "'=BOUND+1)&(" << left(element) << "'=0)&("
           << right(bigson) << "'=" << right(element) << ")&("
           << left(bigson) << "'=" << left(element) << ");" << endl;
      cout << "[checkIf5PosBUG] bintree_invariant & flag_delete & line=7 & (bigson!=element) & root=element & element=" << element
           << " & bigson=" << bigson << " & !(bigson > " << left(element) << " & (bigson < " << right(element) << ")) -> (hit_bug'=true);" << endl;
      for (int father = 1; father < element; ++father) {
        if (father == element || father == bigson) continue;
        cout << "[checkIf5Pos] bintree_invariant & flag_delete & line=7 & (bigson!=element) & " << right(father)
             << "=element & element=" << element
             << " & bigson=" << bigson << " & (bigson > " << left(element) << " & (bigson < " << right(element) << ") & " << father << " < bigson) -> (" << right(father) << "'=bigson)&(line'=9)&("
             << right(element) << "'=BOUND+1)&(" << left(element) << "'=0)&("
             << right(bigson) << "'=" << right(element) << ")&("
             << left(bigson) << "'=" << left(element) << ");" << endl;
        cout << "[checkIf5PosBUG] bintree_invariant & flag_delete & line=7 & (bigson!=element) & " << right(father)
             << "=element & element=" << element
             << " & bigson=" << bigson << " & !(bigson > " << left(element) << " & (bigson < " << right(element) << ") & " << father << " < bigson) -> (hit_bug'=true);" << endl;
      }
      for (int father = element+1; father <= BOUND; ++father) {
        if (father == element || father == bigson) continue;
        cout << "[checkIf5Pos] bintree_invariant & flag_delete & line=7 & (bigson!=element) & " << left(father)
             << "=element & element=" << element
             << " & bigson=" << bigson << " & (bigson > " << left(element) << " & (bigson < " << right(element) << ") & " << father << " > bigson) -> (" << left(father) << "'=bigson)&(line'=9)&("
             << right(element) << "'=BOUND+1)&(" << left(element) << "'=0)&("
             << right(bigson) << "'=" << right(element) << ")&("
             << left(bigson) << "'=" << left(element) << ");" << endl;
        cout << "[checkIf5PosBUG] bintree_invariant & flag_delete & line=7 & (bigson!=element) & " << left(father)
             << "=element & element=" << element
             << " & bigson=" << bigson << " & !(bigson > " << left(element) << " & (bigson < " << right(element) << ") & " << father << " > bigson) -> (hit_bug'=true);" << endl;
      }
    }
  }


  cout << "[checkIf5Neg] bintree_invariant & flag_delete & line=7 & !(bigson!=element) -> (line'=9);" << endl;

//  for (int current = 1; current <= BOUND; ++current) {
//    cout << "[checkIf6Pos] flag_delete & line=8 & current=" << current
//         << " & (current > element) -> (line'=1)&(current'=" << left(current) << ")&(branch'=true);" << endl;
//  }
//  for (int current = 1; current <= BOUND; ++current) {
//    cout << "[checkIf6Neg] flag_delete & line=8 & current=" << current
//         << " & !(current > element) -> (line'=1)&(current'=" << right(current) << ")&(branch'=false);" << endl;
//  }

  cout << "[explicit_invariant_check] line=9 & !hit_bug & !bintree_invariant -> (line'=0)&(hit_bug'=true)&(debug'=true);" << endl;
  cout << "[explicit_invariant_check] line=9 & !hit_bug & bintree_invariant -> (line'=0)&(flag_delete'=false);" << endl;


  cout << "[pre_add] bintree_invariant & element!= 0 & flag_add & line=0 -> (bigson'=root)&(line'=1);" << endl;
  cout << "[checkIf1Pos] bintree_invariant & flag_add & line=1 & (root=0) -> (root'=element)&(line'=0)&(flag_add'=false);" << endl; 
  cout << "[checkIf1Neg] bintree_invariant & flag_add & line=1 & !(root=0) -> (line'=2);" << endl; 
  
  cout << "[checkBoolLoop1Neg] bintree_invariant & flag_add & line=2 & !(bigson!=element) -> (line'=0)&(flag_add'=false);" << endl;

//  cout << "[checkIf2Pos] flag_add & line=3 & (element<bigson) -> (line'=4);" << endl;
//  cout << "[checkIf2Neg] flag_add & line=3 & !(element<bigson) -> (line'=5);" << endl;
  
  for (int current=1; current <= BOUND; ++current) {
    cout << "[checkBoolLoop1PosIf2PosIf3Pos] bintree_invariant & flag_add & line=2 & (element<bigson) "
         << " & bigson=" << current << " & (" << left(current) << "=0) -> (line'=2)&(" << left(current) << "'=element);" << endl;
  }
  for (int current=1; current <= BOUND; ++current) {
    cout << "[checkBoolLoop1PosIf2PosIf3Neg] bintree_invariant & flag_add & line=2 & (element<bigson) "
         << " & bigson=" << current << " & !(" << left(current) << "=0) -> (line'=2)&(bigson'=" << left(current) << ");" << endl;
  }
  for (int current=1; current <= BOUND; ++current) {
    cout << "[checkBoolLoop1PosIf2NegIf3Pos] bintree_invariant & flag_add & line=2 & (element>bigson) "
         << " & bigson=" << current << " & (" << right(current) << "=BOUND+1) -> (line'=2)&(" << right(current) << "'=element);" << endl;
  }
  for (int current=1; current <= BOUND; ++current) {
    cout << "[checkBoolLoop1PosIf2NegIf3Neg] bintree_invariant & flag_add & line=2 & (element>bigson) "
         << " & bigson=" << current << " & !(" << right(current) << "=BOUND+1) -> (line'=2)&(bigson'=" << right(current) << ");" << endl;
  }


//  for (int current=1;current<=BOUND; ++current) {
//    cout << "[checkIf3Pos] flag_add & line=4 & bigson=" << current << " & (" << left(current) << "=0) -> (line'=2)&(" << left(current) << "'=element);" << endl;
//  }
//  for (int current=1;current<=BOUND; ++current) {
//    cout << "[checkIf3Neg] flag_add & line=4 & bigson=" << current << " & !(" << left(current) << "=0) -> (line'=2)&(bigson'=" << left(current) << ");" << endl;
//  }

//  for (int current=1;current<=BOUND; ++current) {
//    cout << "[checkIf4Pos] flag_add & line=5 & bigson=" << current << " & (" << right(current) << "=0) -> (line'=2)&(" << right(current) << "'=element);" << endl;
//  }
//  for (int current=1;current<=BOUND; ++current) {
//    cout << "[checkIf4Neg] flag_add & line=5 & bigson=" << current << " & !(" << right(current) << "=0) -> (line'=2)&(bigson'=" << right(current) << ");" << endl;
//  }
  

//  cout << "[check_bintree_invariant] !hit_bug & !bintree_invariant -> (hit_bug'=true); " << endl;
//  cout << "[check_bintree_invariant] !hit_bug & !bintree_invariant -> (hit_bug'=true); " << endl;



  cout << "endmodule" << endl << endl;
  cout << "rewards" << endl;
  cout << "[tick] true : 1;" << endl;
  cout << "endrewards" << endl;
  return 0;
}
