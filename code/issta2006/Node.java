package issta2006;

public class Node {
	public int value;

	public Node left, right;

	public Node(int x) {
		value = x;
		left = null;
		right = null;
	}

}
