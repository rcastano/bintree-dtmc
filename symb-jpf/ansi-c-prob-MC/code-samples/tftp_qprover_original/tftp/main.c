/*	$OpenBSD: main.c,v 1.4 1997/01/17 07:13:30 millert Exp $	*/
/*	$NetBSD: main.c,v 1.6 1995/05/21 16:54:10 mycroft Exp $	*/

/*
 * Copyright (c) 1983, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* NOTE: This code has been adapted to an academic benchmark 
   for probabilistic software verification. */

#include "tftpsubs.h"

/* Many bug fixes are from Jim Guyton <guyton@rand-unix> */

/*
 * TFTP User Program -- Command Interface.
 */

#include <sys/file.h>
//#include <ctype.h>
#include <netdb.h>

#include "extern.h"

#define	LBUFLEN		200		/* size of input buffer */

struct modes {
  const char *m_name;
  const char *m_mode;
  int m_openflags;
};



static const struct modes modes[] = {
	{ "netascii", "netascii", O_TEXT },
	{ "ascii",    "netascii", O_TEXT },
	{ "octet",    "octet",    O_BINARY },
	{ "binary",   "octet",    O_BINARY },
	{ "image",    "octet",    O_BINARY },
	{ 0, 0, 0 }
};
#define MODE_OCTET    (&modes[2])
#define MODE_NETASCII (&modes[0])
#define MODE_DEFAULT  MODE_NETASCII

struct	sockaddr_in peeraddr;
int	f;
u_short port;
int	trace;
int	verbose;
int	literal;
int	connected;
const struct modes *mode;
int	margc;
char	*margv[20];
const char *prompt = "tftp> ";
void	intr(int);
struct	servent *sp;
int portrange = 0;
unsigned int portrange_from = 0;
unsigned int portrange_to   = 0;

static void settftpmode(const struct modes *newmode);

void	setpeer (const char *);
void	modecmd (const char *);
void	put (const char *);
void    putusage (char *s);
void	status ();
void	quit ();

#define HELPINDENT (sizeof("connect"))

#ifndef __CPROVER__
char	*tail(char *);
#endif

static inline void usage(int errcode)
{
  fprintf(stderr, "Usage: %s [-v][-l][-m mode] [host [port]] [-c command]\n", "tftp");  
  exit(errcode);  
}

int
main()
{
  struct sockaddr_in s_in;
   
  mode = MODE_DEFAULT;
  sp = getservbyname("tftp", "udp");
  
  if (sp == 0) {
    perror("tftp: tftp/udp: unknown service");    
    exit(EX_OSERR);   
  }

  port = sp->s_port;		/* Default port */

  f = socket(AF_INET, SOCK_DGRAM, 0);
  if (f < 0) {
    perror("tftp: socket");
    exit(EX_OSERR);
  }

  verbose = 1;

  s_in.sin_family = 0; 
  s_in.sin_port = 0;
  s_in.sin_addr.s_addr = 0;
  
  s_in.sin_family = AF_INET;

  if (pick_port_bind(f, &s_in, portrange_from, portrange_to)) {
    perror("tftp: bind");    
    exit(EX_OSERR);
  }

  settftpmode(MODE_OCTET);

  setpeer("localhost");  
  status();
  put("test");		
  quit();
  
  return 0;		/* Never reached */
}

char    *hostname;

void
setpeer(const char * h_name)
{
        struct hostent *host;

	host = gethostbyname(h_name);
	if (host == 0) {
		connected = 0;
		printf("%s: unknown host\n", h_name);
		return;
	}
	peeraddr.sin_family = host->h_addrtype;
        _bcopy(host->h_addr, &peeraddr.sin_addr, host->h_length);
	hostname = xstrdup(host->h_name);

	port = sp->s_port;
	
	if (1) {
		printf("Connected to %s (%s), port %u\n",
		       hostname, inet_ntoa(peeraddr.sin_addr),
		       (unsigned int)ntohs(port));
	}
	connected = 1;
}

/*
 * Send file(s).
 */
void
put(const char * file)
{
  	int fd;
	char *cp;

	if (file == NULL) {
		putusage("tftp");
	}

	if (!connected) {
		printf("No target machine specified.\n");
		return;
	}
	
	fd = open(file, O_RDONLY|mode->m_openflags);
	if (fd < 0) {
		fprintf(stderr, "tftp: "); perror(cp);
		return;
	}
		
	if (verbose)
		printf("putting %s to %s:%s [%s]\n", file, hostname, file, mode->m_mode);

	peeraddr.sin_port = port;
	tftp_sendfile(fd, file, mode->m_mode);
	return;
}


// not used
void
modecmd(const char * mode)
{
	const struct modes *p;
	const char *sep;

	for (p = modes; p->m_name; p++)
		if (strcmp(mode, p->m_name) == 0)
			break;
		
	if (p->m_name) {
		settftpmode(p);
		return;
	}
	
	printf("%s: unknown mode\n", mode);
		
	printf("usage: %s [", mode);
	sep = " ";
	for (p = modes; p->m_name; p++) {
		printf("%s%s", sep, p->m_name);
		if (*sep == ' ')
			sep = " | ";
	}
	printf(" ]\nmemcpy");
	return;
}

void
status()
{
	if (connected)
		printf("Connected to %s.\n", hostname);
	else
		printf("Not connected.\n");
	printf("Mode: %s Verbose: %s Tracing: %s Literal: %s\n", mode->m_mode,
		verbose ? "on" : "off", trace ? "on" : "off", literal ? "on" : "off");
}

void
putusage(char *s)
{
	printf("usage: %s file ... target (when already connected)\n", s);
}

static void
settftpmode(const struct modes *newmode)
{
	mode = newmode;
	if (verbose)
		printf("mode set to %s\n", mode->m_mode);
}

void
quit()
{		
	exit(0);
}


