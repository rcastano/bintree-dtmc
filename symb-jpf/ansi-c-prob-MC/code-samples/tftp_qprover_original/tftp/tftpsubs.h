/* $Id$ */

/* $OpenBSD: tftpsubs.h,v 1.2 1996/06/26 05:40:37 deraadt Exp $	*/
/* $NetBSD: tftpsubs.h,v 1.2 1994/12/08 09:51:32 jtc Exp $	*/

/*
 * Copyright (c) 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)tftpsubs.h	8.1 (Berkeley) 6/6/93
 */

/*
 * Prototypes for read-ahead/write-behind subroutines for tftp user and
 * server.
 */
#ifndef TFTPSUBS_H
#define TFTPSUBS_H

#include <stdlib.h>
#include <stdio.h>
#include <sysexits.h>
#include <errno.h>
#include <string.h>
#include <strings.h>

#include <sys/socket.h>
#include <sys/time.h>
#include <sys/fcntl.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/tftp.h>
#include <sys/types.h>

#include "../qprover.h"

/* Some broken systems care about text versus binary, but
   real Unix systems don't... */
#ifndef HAVE_O_TEXT_DEFINITION
#define O_TEXT		0
#endif
#ifndef HAVE_O_BINARY_DEFINITION
#define O_BINARY	0
#endif

#ifdef __CPROVER__
#ifndef PROB_PKT_LOSS
#error "undefined parameter"
#endif
#endif

#define TIMEOUT 	5	
#define TIMEOUT_RATIO 	5
#define	rexmtval 	TIMEOUT
#define	maxtimeout 	(TIMEOUT_RATIO * TIMEOUT)

typedef unsigned short u_short;

#if 1
/* minimalistic state of tftp server */
static int pkt_at_svr = 0;
static u_short dp_opcode;//, dp_block;
#endif 

#ifdef __CPROVER__
#ifndef __PROP1__ // probability of sending WRQ
#ifndef __PROP2__ // probability of sending some of the file
#ifndef __COST__  
#error "no property specified"
#endif
#endif
#endif
#endif

struct tftphdr;

struct tftphdr *r_init(void);
void	read_ahead(FILE *, int);
int	readit(FILE *, struct tftphdr **, int);
int    _readit(FILE *, struct tftphdr **, int);

int	synchnet(int);

ssize_t _sendto(int s, struct tftphdr *buf, size_t len, int flags,
        struct sockaddr *to, socklen_t tolen);

ssize_t _recvfrom(int s, struct tftphdr *buf, size_t len, int flags,
        struct sockaddr *from, socklen_t *fromlen);



extern int segsize;
#define SEGSIZE 	512
#define MAX_SEGSIZE	65464

int pick_port_bind(int sockfd, struct sockaddr_in *myaddr, unsigned int from, unsigned int to);

#ifdef __CPROVER__
#define _ntohs(netshort) netshort
#define _htons(netshort) netshort
#else
#define _ntohs(netshort) ntohs(netshort)
#define _htons(netshort) htons(netshort)
#endif

FILE *_fdopen (int fildes, const char *mode);
char *xstrdup(const char *s);
void _bcopy(const void *src, void *dest, size_t n);

#endif
