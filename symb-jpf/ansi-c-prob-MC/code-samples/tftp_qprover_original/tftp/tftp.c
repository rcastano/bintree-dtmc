/* $OpenBSD: tftp.c,v 1.4 1997/08/06 06:43:45 deraadt Exp $	*/
/* $NetBSD: tftp.c,v 1.5 1995/04/29 05:55:25 cgd Exp $	*/

/*
 * Copyright (c) 1983, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Many bug fixes are from Jim Guyton <guyton@rand-unix> */

/*
 * TFTP User Program -- Protocol Machines
 */
#include "extern.h"
#include "tftpsubs.h"

extern  struct sockaddr_in peeraddr;	/* filled in by main */
extern  int     f;			/* the opened socket */
extern  int     trace;
extern  int     verbose;

#define PKTSIZE SEGSIZE+4
char    ackbuf[PKTSIZE];
int	timeout;

static void nak(int, const char *);
static int makerequest(int, const char *, struct tftphdr *, const char *);
static void printstats(const char *, unsigned long);
static void startclock(void);
static void stopclock(void);
static void tpacket(const char *, struct tftphdr *, int);

/*
 * Send the requested file.
 */
void
tftp_sendfile(int fd, const char *name, const char *mode)
{
	struct tftphdr *ap;	   /* ack packets */
	struct tftphdr *dp;        /* data packets */
	int n;
	volatile int is_request;
	volatile u_short block;
	volatile int size;
	volatile off_t amount;
	struct sockaddr_in from;
	socklen_t fromlen;
	FILE *file;
	u_short ap_opcode, ap_block;

	startclock();		/* start stat's clock */
	dp = r_init();		/* reset fillbuf/read-ahead code */
	ap = (struct tftphdr *)ackbuf;
	file = _fdopen(fd, "rt");
	block = 0;
	is_request = 1;		/* First packet is the actual WRQ */
	amount = 0;
        
	do {
		
		
 		if (is_request) {
			size = makerequest(WRQ, name, dp, mode) - 4;
		} else {	
			#ifdef __COST__
			unit_cost();
			#endif

			size = readit(file, &dp, 0);
			if (size < 0) {
				nak(errno + 100, NULL);
				break;
			}
			dp->th_opcode = _htons((u_short)DATA);
			dp->th_block  = _htons((u_short)block);
		}
		
		timeout = 0;
timeout:
		if (trace)
			tpacket("sent", dp, size + 4);

		n = _sendto(f, dp, size + 4, 0, (struct sockaddr *)&peeraddr, sizeof(peeraddr));
		if (n != size + 4) {
			perror("tftp: sendto");
			goto abort;
		}
		
		read_ahead(file, 0);

		for ( ; ; ) {		
                        // from    = address
			// fromlen = size of address
			fromlen = sizeof(from);

                        n = _recvfrom(f, (struct tftphdr *)ackbuf, sizeof(ackbuf), 0,
			    (struct sockaddr *)&from, &fromlen);
                        
			if (n < 0)
			{
				timeout += rexmtval;
				if (timeout >= maxtimeout)
 				{
					perror("tftp: recvfrom");
					goto abort;
				}

				#ifdef __COST__ 
				F(1);
				#endif
	
				goto timeout;  
			}       

			peeraddr.sin_port = from.sin_port;

			if (trace)
				tpacket("received", ap, n);
		
			// should verify packet came from server 
			ap_opcode = _ntohs((u_short)ap->th_opcode);
			ap_block = _ntohs((u_short)ap->th_block);

			if (ap_opcode == ERROR) {
				printf("Error code %d: %s\n", ap_block,
					ap->th_msg);
				goto abort;
			}

			if (ap_opcode == ACK) {
				int j;

				#ifdef __PROP1__
				F(is_request);
				#endif 	
		
				#ifdef __PROP2__ 
				F(!is_request);
				#endif				
				break;
			}

			// RFC1129/RFC1350: We MUST NOT re-send the DATA
			// packet in response to an invalid ACK.  Doing so
			// would cause the Sorcerer's Apprentice bug.
		}
		if ( !is_request )
			amount += size;
		is_request = 0;
		block++; 
	} while (size == SEGSIZE || block == 1);
abort:
	#ifdef __COST__ 
	F(1);
	#endif
	fclose(file);
	stopclock();
	if (amount > 0)
		printstats("Sent", amount);
}

static int
makerequest(int request, const char *name,
	    struct tftphdr *tp, const char *mode)
{
	char *cp;

	tp->th_opcode = _htons((u_short)request);
	cp = (char *) &(tp->th_stuff);
	
	#ifndef __CPROVER__
	strcpy(cp, name);
	cp += strlen(name);
	*cp++ = '\0';
	strcpy(cp, mode);
	cp += strlen(mode);
	*cp++ = '\0';
	#endif

	return (cp - (char *)tp);
}

static const char * const errmsgs[] =
{
  "Undefined error code", 			/* 0 - EUNDEF */
  "File not found",				/* 1 - ENOTFOUND */
  "Access denied",				/* 2 - EACCESS */
  "Disk full or allocation exceeded", 		/* 3 - ENOSPACE */
  "Illegal TFTP operation",			/* 4 - EBADOP */
  "Unknown transfer ID",			/* 5 - EBADID */
  "File already exists",			/* 6 - EEXISTS */
  "No such user",				/* 7 - ENOUSER */
  "Failure to negotiate RFC2347 options" 	/* 8 - EOPTNEG */
};
#define ERR_CNT (sizeof(errmsgs)/sizeof(const char *))

/*
 * Send a nak packet (error message).
 * Error code passed in is one of the
 * standard TFTP codes, or a UNIX errno
 * offset by 100.
 */
static void
nak(int error, const char *msg)
{  
  struct tftphdr *tp;
  int length;
  
  tp = (struct tftphdr *)ackbuf;
  tp->th_opcode = (u_short)ERROR;
  tp->th_code = (u_short)error;

  if ( error >= 100 ) {
    /* This is a Unix errno+100 */
    if ( !msg )
      msg = strerror(error - 100);
    error = EUNDEF;
  } else {
    if ( (unsigned)error >= ERR_CNT )
      error = EUNDEF;
    
    if ( !msg )
      msg = errmsgs[error];
  }

  tp->th_code = (u_short)error;

  length = 4;			/* Space for header */

  if (trace)
    tpacket("sent", tp, length);
  if (_sendto(f, (struct tftphdr *)ackbuf, length, 0, (struct sockaddr *)&peeraddr,
	     sizeof(peeraddr)) != length)
    perror("nak");
}

static void
tpacket(const char *s, struct tftphdr *tp, int n)
{
	static const char *opcodes[] =
	   { "#0", "RRQ", "WRQ", "DATA", "ACK", "ERROR", "OACK" };
	char *cp, *file;
	u_short op = (u_short)tp->th_opcode;
}

struct timeval tstart;
struct timeval tstop;

static void
startclock(void)
{
	(void)gettimeofday(&tstart, NULL);
}

static void
stopclock(void)
{
	(void)gettimeofday(&tstop, NULL);
}

static void
printstats(const char *direction, unsigned long amount)
{
	double delta;

	delta = (tstop.tv_sec+(tstop.tv_usec/100000.0)) -
		(tstart.tv_sec+(tstart.tv_usec/100000.0));
	if (verbose) {
	  printf("%s %lu bytes in %.1f seconds", direction, amount, delta);
	  printf(" [%.0f bit/s]", (amount*8.)/delta);
	  putchar('\n');
	}
}

