/* tftp-hpa: $Id$ */

/* $OpenBSD: tftpsubs.c,v 1.2 1996/06/26 05:40:36 deraadt Exp $	*/
/* $NetBSD: tftpsubs.c,v 1.3 1994/12/08 09:51:31 jtc Exp $	*/

/*
 * Copyright (c) 1983, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "tftpsubs.h"

/* Simple minded read-ahead/write-behind subroutines for tftp user and
   server.  Written originally with multiple buffers in mind, but current
   implementation has two buffer logic wired in.

   Todo:  add some sort of final error check so when the write-buffer
   is finally flushed, the caller can detect if the disk filled up
   (or had an i/o error) and return a nak to the other side.

			Jim Guyton 10/85
 */

#define PKTSIZE MAX_SEGSIZE+4       /* should be moved to tftp.h */

int segsize = SEGSIZE;		/* Default segsize */

struct bf {
	int counter;            /* size of data in buffer, or flag */
	char buf[PKTSIZE];      /* room for data packet */
} buffer;

				/* Values for bf.counter  */
				/* control flags for crlf conversions */
int newline = 0;		/* fillbuf: in middle of newline expansion */
int prevchar = -1;		/* putbuf: previous char (cr check) */

static struct tftphdr *rw_init(int);

struct tftphdr *r_init() { return rw_init(1); }         /* read-ahead */

/* init for either read-ahead or write-behind */
/* x == zero for write-behind, one for read-head */
static struct tftphdr *
rw_init(int x)
{
	buffer.counter =  0;     /* pass out the first buffer */
	return (struct tftphdr *)buffer.buf;
}

/* Have emptied current buffer by sending to net and getting ack.
   Free it and return next buffer filled with data.
 */
int
readit(FILE *file, struct tftphdr **dpp, int convert)
{
	
	struct bf *b;
	b = &buffer;
	read_ahead(file, convert);
	*dpp = (struct tftphdr *)b->buf;        /* set caller's ptr */

	#ifndef __COST__
	return b->counter;	
        #else
	return SEGSIZE;
	#endif
}

/*
 * fill the input buffer, doing ascii conversions if requested
 * conversions are  lf -> cr,lf  and cr -> cr, nul
 */
void
read_ahead(FILE *file, int convert)
{
	int i;
	char *p;
	int c;
	struct bf *b;
	struct tftphdr *dp;

	b = &buffer;              /* look at "next" buffer */
	dp = (struct tftphdr *)b->buf;
	p = dp->th_data;

	/* empty file, no data */

	b->counter = (int)(p - dp->th_data);
}

/* When an error has occurred, it is possible that the two sides
 * are out of synch.  Ie: that what I think is the other side's
 * response to packet N is really their response to packet N-1.
 *
 * So, to try to prevent that, we flush all the input queued up
 * for us on the network connection on our host.
 *
 * We return the number of packets we flushed (mostly for reporting
 * when trace is active).
 */

int
synchnet(int f)		/* socket to flush */
{
  int pktcount = 0;
  return pktcount;		/* Return packets drained */
}


int pick_port_bind(int sockfd, struct sockaddr_in *myaddr, unsigned int port_range_from, unsigned int port_range_to)
{
  unsigned int port, firstport;
  int port_range = 0;

  if (port_range_from != 0 && port_range_to != 0) {
      port_range = 1;
  }

  firstport = port_range
    ? port_range_from + rand() % (port_range_to-port_range_from+1)
    : 0;

  // pick first port
  port = firstport;
  myaddr->sin_port = htons(port);  

  return 0;
}

ssize_t _sendto(int s, struct tftphdr *buf, size_t len, int flags,
        struct sockaddr *to, socklen_t tolen)
{
	#ifdef __CPROVER__
        

	/** probabilistic choice */
	if (!prob_bool_bias(PROB_PKT_LOSS))
	{
		/** peek at packet (for the server) */
		pkt_at_svr = 1;	
	
		dp_opcode = _ntohs(buf->th_opcode);
		/*if (dp_opcode == WRQ)
			dp_block = 0;
		else
			dp_block = _ntohs(((struct tftphdr *)buf)->th_block);*/

		printf("sendto dp_opcode=%u\n",  dp_opcode);
		//printf("sendto dp_block=%u\n",  dp_block);
	}
	return len;	
	#else
	return sendto(s, buf, len, flags, to, tolen);
	#endif
}

ssize_t _recvfrom(int s, struct tftphdr *buf, size_t len, int flags,
        struct sockaddr *from, socklen_t *fromlen)
{
        #ifdef __CPROVER__
	/** probabilistic choice */
	printf("pkt_at_svr=%d\n", pkt_at_svr);
	
	if (!pkt_at_svr || prob_bool_bias(PROB_PKT_LOSS)) 
	{
		pkt_at_svr = 0;
		return -1;
	}
	/** tftpd (tftp server implementation) */
	else 
	{	
		pkt_at_svr = 0;		
	 
		struct tftphdr *ap = buf;

		printf("recvfrom dp_opcode=%u\n", dp_opcode);
		//printf("recvfrom dp_block=%u\n", dp_block);

	 	if (dp_opcode == WRQ || dp_opcode == DATA) 
		{				
			ap->th_opcode = _htons((u_short)ACK);
			ap->th_block = 0;
			return 4;
		}
		return -1;
	}
	#else
	int n;
	      
  	do {
                *fromlen = sizeof(*from);
		n = recvfrom(s, buf, len, flags,
		    from, fromlen);
	} while (n <= 0);
	
        return n;
	#endif
}

/** Modelled in qprover as uninterpreted functions **/
#ifndef __CPROVER__
FILE *_fdopen (int fildes, const char *mode)
{
	return fdopen(fildes, mode);
}

void _bcopy(const void *src, void *dest, size_t n)
{
	bcopy(src,dest,n);
}

char *xstrdup(const char *s)
{

  char *p = strdup(s);

  if ( !p ) {
    fprintf(stderr, "Out of memory!\n");
    exit(128);
  }

  return p;
}
#endif
