/* Copyright (C) 1998,2001, 2002, 2005, 2007 Free Software Foundation, Inc.

   This file is part of GNU Inetutils.

   GNU Inetutils is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Inetutils is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Inetutils; see the file COPYING.  If not, write
   to the Free Software Foundation, Inc., 51 Franklin Street,
   Fifth Floor, Boston, MA 02110-1301 USA. */

/* NOTE: This code has been adapted to an academic benchmark 
   for probabilistic software verification. */

#include <stdlib.h>
#include <stdio.h>

#include <arpa/inet.h>    // defines nsqrt

#include <signal.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>

#include "icmp.h"         // defines icmphdr_t
#include "ping.h"         // defines ping_data (PING), ping_stat

static int handler (int code, void *closure,
  struct sockaddr_in *dest, struct sockaddr_in *from,
  struct ip *ip, icmphdr_t *icmp, int datalen);

/** FROM: "ping_echo.h" */
int print_echo (int dup, struct ping_stat *stat,
  struct sockaddr_in *dest, struct sockaddr_in *from,
  struct ip *ip, icmphdr_t *icmp, int datalen);
static void print_ip_opt (struct ip *ip, int hlen);
static char * ipaddr2str (struct in_addr ina);
void print_icmp_header (struct sockaddr_in *from,
  struct ip *ip, icmphdr_t *icmp, int len);

/** FROM: "ping_common.h" */
void tvsub (struct timeval *out, struct timeval *in);

static int echo_finish (void);
static int send_echo (PING *ping);

PING *ping;
u_char *data_buffer;
size_t data_length = PING_DATALEN;
unsigned options;
unsigned long preload = 0;
_Bool packet_out;

int main()
{
  PING p;
  ping = &p;

  int fd;
  struct protoent *proto;

  /* initialize raw ICMP socket */
  proto = getprotobyname ("icmp");
  if (!proto)
  {
    fprintf (stderr, "ping: unknown protocol icmp.\n");
    return 1;
  }

  /* open a socket */
  fd = (int) _socket (AF_INET, SOCK_RAW, proto->p_proto);
  if (fd < 0)
  {
    if (errno == EPERM)
      fprintf (stderr, "ping: ping must run as root\n");
    return 1;
  }
  
  /* initialise ping_data */
  ping->ping_fd         = fd;
  ping->ping_type       = ICMP_ECHO;
  ping->ping_count      = 0;
  ping->ping_interval   = PING_DEFAULT_INTERVAL;
  ping->ping_datalen    = sizeof (icmphdr_t);
  ping->ping_ident      = getpid () & 0xFFFF;
  ping->ping_event      = 0;
  ping->ping_closure    = 0;     

  ping->ping_cktab_size = PING_CKTABSIZE;
  ping->ping_cktab      = 0;  
  ping->ping_buffer     = 0;

  ping->ping_num_xmit   = 0; 
  ping->ping_num_recv   = 0;   
  ping->ping_num_rept   = 0;  

  ping->ping_from.sin_family = 0;
  ping->ping_from.sin_port = 0;
  ping->ping_from.sin_addr.s_addr = 0;

  struct ping_stat ping_stat;
 
  ping_stat.tmin = 999999999.0;
  ping_stat.tmax = 0.0;
  ping_stat.tsum = 0.0;
  ping_stat.tsumsq = 0.0;

  /* model count as a non-deterministic choice by the user (1-2)*/
  ping_set_type (ping, ICMP_ECHO);
  ping_set_count (ping, COUNT);
  ping_set_event_handler (ping, handler, &ping_stat);
  ping_set_dest (ping, "www.australia.gov.au");
    
  printf ("PING %s (%s): %d data bytes\n",
	 ping->ping_hostname,
	 inet_ntoa (ping->ping_dest.sin_addr),
	 data_length);
  
  return ping_run (ping, &echo_finish);
}

int
ping_run (PING *ping, int (*finish)())
{
  fd_set fdset;
  int fdmax;
  struct timeval timeout;
  struct timeval last, intvl, now;
  struct timeval *t = NULL;
  int finishing = 0;
  int nresp = 0;
  
  fdmax = ping->ping_fd+1;

  while (preload--)      
    send_echo (ping);

  if (options & OPT_FLOOD)
    {
      intvl.tv_sec = 0;
      intvl.tv_usec = 10000;
    }
  else
    PING_SET_INTERVAL(intvl, ping->ping_interval);
  
  gettimeofday (&last, NULL);
  send_echo (ping);

  while (1)
    {
      int n;
      
      #ifdef __COST__
      F(ping->ping_num_recv > 0);  
      #endif 

      // stops when any packet arrives, or on timeout
      n = _select (fdmax, &fdset, NULL, NULL, &timeout);

      if (n < 0)
	{
	  if (errno != EINTR)
            perror ("select");
          continue;
	}
      // a packet arrived, dont wait next time round
      else if (n == 1)
	{
       
	  if (ping_recv (ping) == 0)
            nresp++;
          
          packet_out = 0;
         
          if (t == 0)
	    {
	      gettimeofday (&now, NULL);
	      t = &now;
	    }
        
          if (ping->ping_count && nresp >= ping->ping_count)
            break;         
	}
      // timeout, send another ping if we have some left, reset last
      else
	{
          // send another ping?
	  if (!ping->ping_count || ping->ping_num_xmit < ping->ping_count)
	    {
	      send_echo (ping);
              if (!(options & OPT_QUIET) && options & OPT_FLOOD)
		putchar ('.');
	    }
          // no more pings to send and we are finishing
	  else if (finishing)
	    break;
          // no more pings, we wait a MAXWAIT interval for any remaining pings out there
	  else 
 	    {
	      finishing = 1;
              intvl.tv_sec = MAXWAIT;
	    }

          // always reset last on timeout
	  gettimeofday (&last, NULL);
	}
    }
  if (finish)
    return (*finish)();
  return 0;
}

static int
echo_finish ()
{
  ping_finish ();
  if (ping->ping_num_recv && PING_TIMING (data_length))
    {
      struct ping_stat *ping_stat = (struct ping_stat*)ping->ping_closure;      
      printf ("round-trip min/max = %.3f/%.3f ms\n",
	      ping_stat->tmin,	      
	      ping_stat->tmax);     
    }

  // what is the probability of receiving ALL
  #ifdef __PROP1__
  F(ping->ping_num_recv == COUNT && 
    ping->ping_num_xmit == COUNT);  
  #endif

  // what is the probability of receiving some but not all.
  #ifdef __PROP2__
  F(ping->ping_num_recv > 0 && 
    ping->ping_num_recv < ping->ping_num_xmit && 
    ping->ping_num_xmit == COUNT);
  #endif

  // what is the probability of receiving NONE
  #ifdef __PROP3__
  F(ping->ping_num_recv == 0 && 
    ping->ping_num_xmit == COUNT);  
  #endif

  // exit 
  exit (ping->ping_num_recv == 0);
}

int
handler (int code, void *closure,
	struct sockaddr_in *dest, struct sockaddr_in *from,
	struct ip *ip, icmphdr_t *icmp, int datalen)
{
  switch (code)
    {
    case PEV_RESPONSE:
    case PEV_DUPLICATE:
      print_echo (code == PEV_DUPLICATE,
		   (struct ping_stat*)closure, dest, from, ip, icmp, datalen);
      break;
    }
  return 0;
}

static int
send_echo (PING *ping)
{
  int off = 0;
  
  #ifdef __COST__
  unit_cost();
  #endif
  packet_out = 1;
  return ping_xmit (ping);
}

int
print_echo (int dupflag, struct ping_stat *ping_stat,
	   struct sockaddr_in *dest, struct sockaddr_in *from,
	   struct ip *ip, icmphdr_t *icmp, int datalen)
{
  // we assume this function is only used for printing
  int hlen;
  struct timeval tv;
  int timing = 0;
  double triptime = 0.0;

  // Length of ICMP header+payload 
  datalen -= hlen;

  printf ("%d bytes: icmp_seq=%u", datalen, icmp->icmp_seq);
  printf (" ttl=%d", ip->ip_ttl);
  if (dupflag)
    printf (" (DUP!)");

  printf ("\n");
 
  return 0;
}

int
ping_finish ()
{
  /*printf("ping_finish\n");
  
  fflush (stdout);
  printf ("--- %s ping statistics ---\n", ping->ping_hostname);
  printf ("%ld packets transmitted, ", ping->ping_num_xmit);
  printf ("%ld packets received, ", ping->ping_num_recv);

  if (ping->ping_num_xmit)
    {
      if (ping->ping_num_recv > ping->ping_num_xmit)
	printf ("-- somebody's printing up packets!");
      else
	printf ("%d%% packet loss",
		(int) (((ping->ping_num_xmit - ping->ping_num_recv) * 100) /
		       ping->ping_num_xmit));

    }
  printf ("\n");*/
  return 0;
}

/** FROM: "ping_common.c" */
void
tvsub (struct timeval *out, struct timeval *in)
{
  if ((out->tv_usec -= in->tv_usec) < 0)
    {
      --out->tv_sec;
      out->tv_usec += 1000000;
    }
  out->tv_sec -= in->tv_sec;
}

/** FROM: "ping_echo.c" */
#define NITEMS(a) sizeof(a)/sizeof((a)[0])

struct icmp_diag {
  int type;
  char *text;
  void (*fun) (icmphdr_t *, void *data);
  void *data;
};

void
print_icmp_header (struct sockaddr_in *from,
		   struct ip *ip, icmphdr_t *icmp, int len)
{
  printf ("ICMP type: %d\n", icmp->icmp_type);
}

/** FROM: "ping_echo.c" */
void
print_ip_opt (struct ip *ip, int hlen)
{
}

/** FROM: "ping_echo.h" */

#ifndef __CPROVER__
char *
ipaddr2str (struct in_addr ina)
{
   return "addr";
}
#endif

/** Stub for 'socket' in: "<sys/socket.h>" */
int _socket (int __domain, int __type, int __protocol)
{
  #ifdef __CPROVER__
  return (biased_coin(PROB_SOCKET_FAILS)) ? -1 : 1;
  #else
  return socket(__domain, __type, __protocol);
  #endif
}

/** Stub for 'select' in: "<sys/select.h>" */
int _select(int nfds, fd_set *readfds, fd_set *writefds,
  fd_set *exceptfds, struct timeval *timeout)
{
  #ifdef __CPROVER__
  if (packet_out)
  {
    packet_out = 0;

    // loose packet some probability
    if (!biased_coin(PROB_PING_LOSS))
    {
      return 1;
    }
  }
  return 0;
  #else
  return select(nfds, readfds, writefds, exceptfds, timeout);
  #endif
}



