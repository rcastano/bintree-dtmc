
#include "qprover.h"
#include "stdlib.h"
//#include "string.h"

typedef int bool;
#define true 1
#define false 0
typedef struct Node {
	int value;

	void *left;
  void *right;

  // This is a meta variable. Since qprover 
  // doesn't support dynamic memory, 
  // I have to setup the whole data structure
  // with a full outer layer of 
  // "null" nodes.
  bool in_use;
} Node;
bool out_of_bounds = false;
bool hit_bug = false;

#define N_NODES 8
static Node pool[N_NODES];

Node *makeNode(int x) {
  Node *result = NULL;
  int i;
  for (i = 0; i < N_NODES; ++i) {
    if (!pool[i].in_use) {
      result = &pool[i];
    } 
  }
  if (result != NULL) {
    result->value = x;
    result->left = NULL;
    result->right = NULL;
    result->in_use = true;
  } else {
    out_of_bounds = true;
  }
  return result;
}


typedef struct BinTree {
  Node *root;
} BinTree;

BinTree b;

void setup() {
  int i;
  for (i=0; i < N_NODES; ++i) {
    pool[i].in_use = false;
  }
  b.root = NULL;
}

	bool checkTree2n0(Node *n, int min, int max) {
    return false;
	}
	bool checkTree2n1(Node *n, int min, int max) {
		if (n == NULL)
			return true;
		if (n->value < min || n->value > max)
			return false;
		bool resLeft = checkTree2n0(n->left,min,n->value-1);
		if(!resLeft)
			return false;
		else
			return checkTree2n0(n->right,n->value+1,max);
	}

	bool checkTree2n2(Node *n, int min, int max) {
		if (n == NULL)
			return true;
		if (n->value < min || n->value > max)
			return false;
		bool resLeft = checkTree2n1(n->left,min,n->value-1);
		if(!resLeft)
			return false;
		else
			return checkTree2n1(n->right,n->value+1,max);
	}

	bool checkTree2n3(Node *n, int min, int max) {
		if (n == NULL)
			return true;
		if (n->value < min || n->value > max)
			return false;
		bool resLeft = checkTree2n2(n->left,min,n->value-1);
		if(!resLeft)
			return false;
		else
			return checkTree2n2(n->right,n->value+1,max);
	}

	bool checkTree2n4(Node *n, int min, int max) {
		if (n == NULL)
			return true;
		if (n->value < min || n->value > max)
			return false;
		bool resLeft = checkTree2n3(n->left,min,n->value-1);
		if(!resLeft)
			return false;
		else
			return checkTree2n3(n->right,n->value+1,max);
	}

	bool checkTree2n5(Node *n, int min, int max) {
		if (n == NULL)
			return true;
		if (n->value < min || n->value > max)
			return false;
		bool resLeft = checkTree2n4(n->left,min,n->value-1);
		if(!resLeft)
			return false;
		else
			return checkTree2n4(n->right,n->value+1,max);
	}
	bool checkTree() {
		//return checkTree(root,Integer.MIN_VALUE, Integer.MAX_VALUE);
		return checkTree2n5(b.root,-9, 9);
	}
	
#define node_stack_size N_NODES
Node *node_stack[node_stack_size];
int node_stack_begin = 0;
int node_stack_end = 0;

void node_stack_push_back(Node *node) {
  if (out_of_bounds || ((node_stack_end + 1) % node_stack_size) == node_stack_begin) {
    out_of_bounds = true;
    return ;
  }
  node_stack[node_stack_end] = node;
  ++node_stack_end;
}
void node_stack_push(Node *node) {
  if (out_of_bounds || ((node_stack_end + 1) % node_stack_size) == node_stack_begin) {
    out_of_bounds = true;
    return ;
  }
  node_stack[node_stack_end] = node;
  ++node_stack_end;
}
bool node_stack_empty() {
  return out_of_bounds || node_stack_end == node_stack_begin;
}
Node *node_stack_pop() {
  if (node_stack_empty()) {
    return NULL;
  }
  node_stack_end =  (node_stack_end + node_stack_size - 1) % node_stack_size;
  return node_stack[node_stack_end];
}

void mark(Node *node) {
  node_stack_push(node);
  while(!node_stack_empty()) {
    Node * current = node_stack_pop();
    if (current == NULL) continue;
    node->in_use = true;
    node_stack_push(node->left);
    node_stack_push(node->right);
  }
}

void flush_used() {
  int i;
  for (i = 0; i  < N_NODES; ++i) {
    pool[i].in_use = false;
  }
  mark(b.root);
}




	//----
//	public static void outputTestSequence(int number) {
//	}

//	public native boolean checkAbstractState(int which);
//	public native boolean checkAbstractState2(int which, int x);

	
//	public native static int gen_native(int br, Node n0, int x, Node n1, Node n2);

//	public static void gen(int br, Node n0, int x, Node n1, Node n2) {
///*		int c = gen_native(br, n0, x, n1, n2);
//		if (c != 0)
//			outputTestSequence(c);*/
//	}

	//----

	void add(int x) {
		// line 0
		Node *current = b.root;
		// line 1
		if (b.root == NULL) { // If 1
			b.root = makeNode(x);
			return;
		}
		// line 2
		while (current->value != x) { // Loop 1
			// line 3
			if (x < current->value) { // If 2
				// line 4
				if (current->left == NULL) { // If 3
					current->left = makeNode(x);
				} else {
					current = current->left;
				}
			} else {
				// line 5
				if (current->right == NULL) { // If 4
					current->right = makeNode(x);
				} else {
					current = current->right;
				}
			}
		}
	}


	bool remove_elem(int x) {
		// line 0
		Node *current = b.root;
		Node *parent = NULL;
		bool branch = true; //true =left, false =right
		// line 1
		while (current != NULL) { // Loop 1
			// line 2
			if (current->value == x) { // If 1
				Node *bigson = current;
				// line 3
				while (bigson->left != NULL || bigson->right != NULL) { // Loop 2
					parent = bigson;
					// line 4
					if (bigson->right != NULL) { // If 2
						bigson = bigson->right;
						branch = false;
					} else {
						bigson = bigson->left;
						branch = true;
					}
				}
				// line 5
				//		System.out.println("Remove: current "+current.value+" parent "+parent.value+" bigson "+bigson.value);
				if (parent != NULL) { // If 3
					// line 6
					if (branch) { // If 4
						parent->left = NULL;
					} else {
						parent->right = NULL;
					}
				}
				// line 7
				if (bigson != current) { // If 5
					current->value = bigson->value;
				} else {
				}
				
				return true;
			}
			parent = current;
			//	    if (current.value <x ) { // THERE WAS ERROR
			// line 8
			if (current->value > x) { // If 6
				current = current->left;
				branch = true;
			} else {
				current = current->right;
				branch = false;
			}
		}
		// line 9: check invariant
		return false;
	}


void try_remove(int x) {
  remove_elem(x);
  if (!checkTree()) {
    hit_bug = true;
  }
  flush_used();
}


#include <stdio.h>
//#include "BinTree.h"

int main() {
  //BinTree bintree = new BinTree();
  int ops;
  for (ops = 0; ops < 5; ++ops) {
    bool do_add = __CPROVER_prob_biased_coin(1, 2);
    int elem = __CPROVER_prob_uniform_int(0, 10);
    if (do_add) {
      add(elem);
    } else {
      remove(elem);
    }
  }

  F(!out_of_bounds && !hit_bug);
  //System.out.println(bintree.root.value);
}
