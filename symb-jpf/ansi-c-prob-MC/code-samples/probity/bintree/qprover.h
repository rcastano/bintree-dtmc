#ifndef __QPROVER_BUILTIN_FUNCTIONS
#define __QPROVER_BUILTIN_FUNCTIONS

/* MACROS */
#define TARGET(expr) __CPROVER_assert(!(expr), "target")
#define __CPROVER_coin() __CPROVER_prob_bool(1,2)

#define F(expr) __CPROVER_assert(!(expr), "target")

// returns 1 with probability num/den and probability 0 otherwise
_Bool __CPROVER_prob_biased_coin(unsigned long long num, unsigned long long den);

// pick a number uniformly at random in [lb,ub)
unsigned char  __CPROVER_prob_uniform_uchar (unsigned char  lb, unsigned char  ub);
unsigned short __CPROVER_prob_uniform_ushort(unsigned short lb, unsigned short ub);
unsigned int   __CPROVER_prob_uniform_uint  (unsigned int   lb, unsigned int   ub);
unsigned long  __CPROVER_prob_uniform_ulong (unsigned long  lb, unsigned long  ub);

// pick a number uniformly at random in [lb,ub)
char  __CPROVER_prob_uniform_char (char  lb, char  ub);
short __CPROVER_prob_uniform_short(short lb, short ub);
int   __CPROVER_prob_uniform_int  (int   lb, int   ub);
long  __CPROVER_prob_uniform_long (long  lb, long  ub);

_Bool nondet_bool();

#endif
