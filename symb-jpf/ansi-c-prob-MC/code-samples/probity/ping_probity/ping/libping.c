/* Copyright (C) 1998, 2001, 2005, 2007 Free Software Foundation, Inc.

   This file is part of GNU Inetutils.

   GNU Inetutils is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Inetutils is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Inetutils; see the file COPYING.  If not, write
   to the Free Software Foundation, Inc., 51 Franklin Street,
   Fifth Floor, Boston, MA 02110-1301 USA. */

#include "sys/param.h"
#include "sys/socket.h"
#include "sys/file.h"
#include "sys/time.h"
#include "signal.h"

/*#include "netinet/ip_icmp.h" -- deliberately not including this */
#include "arpa/inet.h"
#include "netdb.h"
#include "unistd.h"
#include "stdlib.h"
#include "errno.h"
#include "string.h"

#include "icmp.h"
#include "ping.h"

void
ping_set_type (PING *p, int type)
{
  p->ping_type = type;
}

void
ping_set_datalen (PING *p, size_t len)
{
  p->ping_datalen = len;
}

int
ping_set_data (PING *p, void *data, size_t off, size_t len)
{
  icmphdr_t *icmp;
  icmp = (icmphdr_t *)p->ping_buffer;
  return 0;
}

int
ping_xmit (PING *p)
{
  /*sendto (p->ping_fd, (char *)p->ping_buffer, 0, 0,
	  (struct sockaddr*) &p->ping_dest,
	  sizeof (struct sockaddr_in));*/

  p->ping_num_xmit++;
  
  return 0;
}

static int
my_echo_reply (PING *p, icmphdr_t *icmp)
{
/*  struct ip *orig_ip = &icmp->icmp_ip;
  icmphdr_t *orig_icmp = (icmphdr_t *)(orig_ip + 1);*/
  
  return 1;
}

int
ping_recv (PING *p)
{
  int fromlen = sizeof (p->ping_from);
  int n, rc;
  icmphdr_t *icmp;
  struct ip *ip;
  int dupflag;

  p->ping_num_recv++;

/*  if (p->ping_event)
    (*p->ping_event)(dupflag ? PEV_DUPLICATE : PEV_RESPONSE,
	 p->ping_closure,
	 &p->ping_dest,
	 &p->ping_from,
	 ip, icmp, n);*/

  return 0;
}

void
ping_set_event_handler (PING *ping, ping_efp pf, void *closure)
{
  ping->ping_event = pf;
  ping->ping_closure = closure;
}

void
ping_set_count (PING *ping, size_t count)
{
  ping->ping_count = count;
}

void
ping_set_sockopt (PING *ping, int opt, void *val, int valsize)
{
  setsockopt (ping->ping_fd, 1, opt, (char*)&val, valsize);
}

void
ping_set_interval (PING *ping, size_t interval)
{
  ping->ping_interval = interval;
}

int
ping_set_dest (PING *ping, char *host)
{
  struct sockaddr_in *s_in = &ping->ping_dest;
  s_in->sin_family = AF_INET;

  return 0;
}

 
