//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
// 
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
// 
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//
//package issta2006.BinTree;

#include "stdlib.h"
//#include "string.h"

typedef int bool;
#define true 1
#define false 0
typedef struct Node {
	int value;

	void *left;
  void *right;

  // This is a meta variable. Since qprover 
  // doesn't support dynamic memory, 
  // I have to setup the whole data structure
  // with a full outer layer of 
  // "null" nodes.
  bool in_use;
} Node;
bool out_of_bounds = false;
bool hit_bug = false;

#define N_NODES 32
static Node pool[N_NODES];

Node *makeNode(int x) {
  Node *result = NULL;
  int i;
  for (i = 0; i < N_NODES; ++i) {
    if (!pool[i].in_use) {
      result = &pool[i];
    } 
  }
  if (result != NULL) {
    result->value = x;
    result->left = NULL;
    result->right = NULL;
    result->in_use = true;
  } else {
    out_of_bounds = true;
  }
  return result;
}


typedef struct BinTree {
  Node *root;
} BinTree;

BinTree b;

void setup() {
  int i;
  for (i=0; i < N_NODES; ++i) {
    pool[i].in_use = false;
  }
  b.root = NULL;
}
	bool checkTree2(Node *n, int min, int max) {
		if (n == NULL)
			return true;
		if (n->value < min || n->value > max)
			return false;
		bool resLeft = checkTree2(n->left,min,n->value-1);
		if(!resLeft)
			return false;
		else
			return checkTree2(n->right,n->value+1,max);
	}
	bool checkTree() {
		//return checkTree(root,Integer.MIN_VALUE, Integer.MAX_VALUE);
		return checkTree2(b.root,-9, 9);
	}
	


void mark(Node *node) {
  if (node == NULL) return ;
  node->in_use = true;
  mark(node->left);
  mark(node->right);
}

flush_used() {
  int i;
  for (i = 0; i  < N_NODES; ++i) {
    pool[i].in_use = false;
  }
  mark(b.root);
}




	//----
//	public static void outputTestSequence(int number) {
//	}

//	public native boolean checkAbstractState(int which);
//	public native boolean checkAbstractState2(int which, int x);

	
//	public native static int gen_native(int br, Node n0, int x, Node n1, Node n2);

//	public static void gen(int br, Node n0, int x, Node n1, Node n2) {
///*		int c = gen_native(br, n0, x, n1, n2);
//		if (c != 0)
//			outputTestSequence(c);*/
//	}

	//----

	void add(int x) {
		// line 0
		Node *current = b.root;
		// line 1
		if (b.root == NULL) { // If 1
			b.root = makeNode(x);
			return;
		}
		// line 2
		while (current->value != x) { // Loop 1
			// line 3
			if (x < current->value) { // If 2
				// line 4
				if (current->left == NULL) { // If 3
					current->left = makeNode(x);
				} else {
					current = current->left;
				}
			} else {
				// line 5
				if (current->right == NULL) { // If 4
					current->right = makeNode(x);
				} else {
					current = current->right;
				}
			}
		}
	}


	bool remove_elem(int x) {
		// line 0
		Node *current = b.root;
		Node *parent = NULL;
		bool branch = true; //true =left, false =right
		// line 1
		while (current != NULL) { // Loop 1
			// line 2
			if (current->value == x) { // If 1
				Node *bigson = current;
				// line 3
				while (bigson->left != NULL || bigson->right != NULL) { // Loop 2
					parent = bigson;
					// line 4
					if (bigson->right != NULL) { // If 2
						bigson = bigson->right;
						branch = false;
					} else {
						bigson = bigson->left;
						branch = true;
					}
				}
				// line 5
				//		System.out.println("Remove: current "+current.value+" parent "+parent.value+" bigson "+bigson.value);
				if (parent != NULL) { // If 3
					// line 6
					if (branch) { // If 4
						parent->left = NULL;
					} else {
						parent->right = NULL;
					}
				}
				// line 7
				if (bigson != current) { // If 5
					current->value = bigson->value;
				} else {
				}
				
				return true;
			}
			parent = current;
			//	    if (current.value <x ) { // THERE WAS ERROR
			// line 8
			if (current->value > x) { // If 6
				current = current->left;
				branch = true;
			} else {
				current = current->right;
				branch = false;
			}
		}
		// line 9: check invariant
		return false;
	}


void try_remove(int x) {
  remove_elem(x);
  if (!checkTree()) {
    hit_bug = true;
  }
}


/*
typedef struct PairDepthNode { 
  int depth;
  Node *n;
} PairDepthNode;
void printBinTree(BinTree *b) {
  exit -1; // Not finished implementing this.
  int size_stack = 20;
  int end_stack = 0;
  PairDepthNode *stack = (PairDepthNode *) malloc (size_stack * sizeof(PairDepthNode));

  int depth = 0;
  int max_depth = 0;
  stack[0].depth = 0;
  stack[0].n = b->root;
  end_stack = 1;
  while(end_stack != 0) {
    Node *current = stack[end_stack-1].n;
    int current_depth = stack[end_stack-1].depth;
    --end_stack;

    if (current_depth > max_depth) {
      max_depth = current_depth;
    }

    if (current->left != NULL) {
      if (end_stack == size_stack) {
        size_stack *= 2;
        PairDepthNode *new_stack = (PairDepthNode *) malloc(size_stack * sizeof(PairDepthNode));
        memcpy(new_stack, stack, (size_stack / 2) * sizeof(PairDepthNode));
        free(stack);
        stack = new_stack;
      }
      stack[end_stack].n = current->left;
      stack[end_stack].depth = current_depth + 1;
      ++end_stack;
    }
  }

  int width = 1;
  for (int temp=depth; temp > 0; --temp) {
    width *= 2;
  }
  bool *grid_pos_empty = (bool *) malloc (width * depth * sizeof(bool));
  int *grid_pos = (int *) malloc (width * depth * sizeof(int));
  
  memset(grid_pos_empty, 0, width * depth * sizeof(bool));
  memset(grid_pos, 0, width * depth * sizeof(int));

  

}
*/
