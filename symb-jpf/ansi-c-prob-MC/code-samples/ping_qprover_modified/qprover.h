#ifndef __QPROVER_BUILTIN_FUNCTIONS
#define __QPROVER_BUILTIN_FUNCTIONS

/* MACROS */

#define coin prob_bool
#define fair_coin prob_bool
#define biased_coin prob_bool_bias
#define nondet_coin nondet_bool

/* If just compiling, only fail when not reaching goal */
#ifdef __CPROVER__
#define F(expr) assert(!(expr))
#define GOAL(expr) assert(!(expr))
#define TARGET(expr) __CPROVER_assert(!(expr), "target")
#else
#include <stdio.h>
#include <stdlib.h>
#define F(expr)    if (expr) { printf("POSITIVE\n"); exit(0); } else { printf("NEGATIVE\n"); }
#define GOAL(expr) if (expr) { printf("POSITIVE\n"); exit(0); } else { printf("NEGATIVE\n"); }
#define TARGET(expr) if (expr) { printf("Reached the objective\n"); exit(0); } 	
#endif

/* SPECIAL FUNCTIONS TO MODEL QUANTITATIVE BEHAVIOUR */

_Bool              nondet_bool();
_Bool              prob_bool();
_Bool              prob_bool_bias(double bias);
void               unit_cost();

unsigned char      prob_uchar();
unsigned char      nondet_uchar();

#ifdef __CPROVER__
unsigned __int1    prob_uint1();
unsigned __int2    prob_uint2();
unsigned __int3    prob_uint3();
unsigned __int4    prob_uint4();
unsigned __int5    prob_uint5();
unsigned __int6    prob_uint6();
unsigned __int7    prob_uint7();
unsigned __int8    prob_uint8();
unsigned __int9    prob_uint9();
unsigned __int10   prob_uint10();
unsigned __int11   prob_uint11();
unsigned __int12   prob_uint12();
unsigned __int13   prob_uint13();
unsigned __int14   prob_uint14();
unsigned __int15   prob_uint15();
unsigned __int16   prob_uint16();
unsigned __int17   prob_uint17();
unsigned __int18   prob_uint18();
unsigned __int19   prob_uint19();
unsigned __int20   prob_uint20();
unsigned __int21   prob_uint21();
unsigned __int22   prob_uint22();
unsigned __int23   prob_uint23();
unsigned __int24   prob_uint24();
unsigned __int25   prob_uint25();
unsigned __int26   prob_uint26();
unsigned __int27   prob_uint27();
unsigned __int28   prob_uint28();
unsigned __int29   prob_uint29();
unsigned __int30   prob_uint30();
unsigned __int31   prob_uint31();
unsigned __int32   prob_uint32();
unsigned __int33   prob_uint33();
unsigned __int34   prob_uint34();
unsigned __int35   prob_uint35();
unsigned __int36   prob_uint36();
unsigned __int37   prob_uint37();
unsigned __int38   prob_uint38();
unsigned __int39   prob_uint39();
unsigned __int40   prob_uint40();
unsigned __int41   prob_uint41();
unsigned __int42   prob_uint42();
unsigned __int43   prob_uint43();
unsigned __int44   prob_uint44();
unsigned __int45   prob_uint45();
unsigned __int46   prob_uint46();
unsigned __int47   prob_uint47();
unsigned __int48   prob_uint48();
unsigned __int49   prob_uint49();
unsigned __int50   prob_uint50();
unsigned __int51   prob_uint51();
unsigned __int52   prob_uint52();
unsigned __int53   prob_uint53();
unsigned __int54   prob_uint54();
unsigned __int55   prob_uint55();
unsigned __int56   prob_uint56();
unsigned __int57   prob_uint57();
unsigned __int58   prob_uint58();
unsigned __int59   prob_uint59();
unsigned __int60   prob_uint60();
unsigned __int61   prob_uint61();
unsigned __int62   prob_uint62();
unsigned __int63   prob_uint63();
unsigned __int64   prob_uint64();

__int1             prob_int1();
__int2             prob_int2();
__int3             prob_int3();
__int4             prob_int4();
__int5             prob_int5();
__int6             prob_int6();
__int7             prob_int7();
__int8             prob_int8();
__int9             prob_int9();
__int10            prob_int10();
__int11            prob_int11();
__int12            prob_int12();
__int13            prob_int13();
__int14            prob_int14();
__int15            prob_int15();
__int16            prob_int16();
__int17            prob_int17();
__int18            prob_int18();
__int19            prob_int19();
__int20            prob_int20();
__int21            prob_int21();
__int22            prob_int22();
__int23            prob_int23();
__int24            prob_int24();
__int25            prob_int25();
__int26            prob_int26();
__int27            prob_int27();
__int28            prob_int28();
__int29            prob_int29();
__int30            prob_int30();
__int31            prob_int31();
__int32            prob_int32();
__int33            prob_int33();
__int34            prob_int34();
__int35            prob_int35();
__int36            prob_int36();
__int37            prob_int37();
__int38            prob_int38();
__int39            prob_int39();
__int40            prob_int40();
__int41            prob_int41();
__int42            prob_int42();
__int43            prob_int43();
__int44            prob_int44();
__int45            prob_int45();
__int46            prob_int46();
__int47            prob_int47();
__int48            prob_int48();
__int49            prob_int49();
__int50            prob_int50();
__int51            prob_int51();
__int52            prob_int52();
__int53            prob_int53();
__int54            prob_int54();
__int55            prob_int55();
__int56            prob_int56();
__int57            prob_int57();
__int58            prob_int58();
__int59            prob_int59();
__int60            prob_int60();
__int61            prob_int61();
__int62            prob_int62();
__int63            prob_int63();
__int64            prob_int64();

unsigned __int1    nondet_uint1();
unsigned __int2    nondet_uint2();
unsigned __int3    nondet_uint3();
unsigned __int4    nondet_uint4();
unsigned __int5    nondet_uint5();
unsigned __int6    nondet_uint6();
unsigned __int7    nondet_uint7();
unsigned __int8    nondet_uint8();
unsigned __int9    nondet_uint9();
unsigned __int10   nondet_uint10();
unsigned __int11   nondet_uint11();
unsigned __int12   nondet_uint12();
unsigned __int13   nondet_uint13();
unsigned __int14   nondet_uint14();
unsigned __int15   nondet_uint15();
unsigned __int16   nondet_uint16();
unsigned __int17   nondet_uint17();
unsigned __int18   nondet_uint18();
unsigned __int19   nondet_uint19();
unsigned __int20   nondet_uint20();
unsigned __int21   nondet_uint21();
unsigned __int22   nondet_uint22();
unsigned __int23   nondet_uint23();
unsigned __int24   nondet_uint24();
unsigned __int25   nondet_uint25();
unsigned __int26   nondet_uint26();
unsigned __int27   nondet_uint27();
unsigned __int28   nondet_uint28();
unsigned __int29   nondet_uint29();
unsigned __int30   nondet_uint30();
unsigned __int31   nondet_uint31();
unsigned __int32   nondet_uint32();
unsigned __int33   nondet_uint33();
unsigned __int34   nondet_uint34();
unsigned __int35   nondet_uint35();
unsigned __int36   nondet_uint36();
unsigned __int37   nondet_uint37();
unsigned __int38   nondet_uint38();
unsigned __int39   nondet_uint39();
unsigned __int40   nondet_uint40();
unsigned __int41   nondet_uint41();
unsigned __int42   nondet_uint42();
unsigned __int43   nondet_uint43();
unsigned __int44   nondet_uint44();
unsigned __int45   nondet_uint45();
unsigned __int46   nondet_uint46();
unsigned __int47   nondet_uint47();
unsigned __int48   nondet_uint48();
unsigned __int49   nondet_uint49();
unsigned __int50   nondet_uint50();
unsigned __int51   nondet_uint51();
unsigned __int52   nondet_uint52();
unsigned __int53   nondet_uint53();
unsigned __int54   nondet_uint54();
unsigned __int55   nondet_uint55();
unsigned __int56   nondet_uint56();
unsigned __int57   nondet_uint57();
unsigned __int58   nondet_uint58();
unsigned __int59   nondet_uint59();
unsigned __int60   nondet_uint60();
unsigned __int61   nondet_uint61();
unsigned __int62   nondet_uint62();
unsigned __int63   nondet_uint63();
unsigned __int64   nondet_uint64();

__int1             nondet_int1();
__int2             nondet_int2();
__int3             nondet_int3();
__int4             nondet_int4();
__int5             nondet_int5();
__int6             nondet_int6();
__int7             nondet_int7();
__int8             nondet_int8();
__int9             nondet_int9();
__int10            nondet_int10();
__int11            nondet_int11();
__int12            nondet_int12();
__int13            nondet_int13();
__int14            nondet_int14();
__int15            nondet_int15();
__int16            nondet_int16();
__int17            nondet_int17();
__int18            nondet_int18();
__int19            nondet_int19();
__int20            nondet_int20();
__int21            nondet_int21();
__int22            nondet_int22();
__int23            nondet_int23();
__int24            nondet_int24();
__int25            nondet_int25();
__int26            nondet_int26();
__int27            nondet_int27();
__int28            nondet_int28();
__int29            nondet_int29();
__int30            nondet_int30();
__int31            nondet_int31();
__int32            nondet_int32();
__int33            nondet_int33();
__int34            nondet_int34();
__int35            nondet_int35();
__int36            nondet_int36();
__int37            nondet_int37();
__int38            nondet_int38();
__int39            nondet_int39();
__int40            nondet_int40();
__int41            nondet_int41();
__int42            nondet_int42();
__int43            nondet_int43();
__int44            nondet_int44();
__int45            nondet_int45();
__int46            nondet_int46();
__int47            nondet_int47();
__int48            nondet_int48();
__int49            nondet_int49();
__int50            nondet_int50();
__int51            nondet_int51();
__int52            nondet_int52();
__int53            nondet_int53();
__int54            nondet_int54();
__int55            nondet_int55();
__int56            nondet_int56();
__int57            nondet_int57();
__int58            nondet_int58();
__int59            nondet_int59();
__int60            nondet_int60();
__int61            nondet_int61();
__int62            nondet_int62();
__int63            nondet_int63();
__int64            nondet_int64();

#endif  // __CPROVER__
/* DO NOT USE */

/*
short int          nondet_short_int();
int                nondet_int();
long int           nondet_long_int();
long long          nondet_long_long();

unsigned short int nondet_ushort_int();
unsigned int       nondet_uint();
unsigned long int  nondet_ulong_int();
unsigned long long nondet_ulong_long();

float              nondet_float();
double             nondet_double();
long double        nondet_long_double();

char               nondet_char();
unsigned char      nondet_uchar();

void *             nondet_pointer();

void               cost_unit();
*/

#endif
