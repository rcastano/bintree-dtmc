#include <vector>

#include "generate_transition.h"

using namespace std;

int main() {
  vector<string> nodes;
  string temp;
//  while (cin >> temp && temp != "end") {
//    nodes.push_back(temp);
//  }
  nodes.push_back("r");
  nodes.push_back("r0");
//  nodes.push_back("r1");
//  nodes.push_back("r11");
  vector<string> neg_vars;
//  neg_vars.push_back("r0");
  neg_vars.push_back("r1");
  neg_vars.push_back("r11");


  neg_vars.push_back("r00");
  neg_vars.push_back("r01");
  neg_vars.push_back("r10");

//  neg_vars.push_back("r000");
//  neg_vars.push_back("r001");
//  neg_vars.push_back("r010");
//  neg_vars.push_back("r011");
//  neg_vars.push_back("r100");
//  neg_vars.push_back("r101");
//  neg_vars.push_back("r110");
//  neg_vars.push_back("r111");

  vector<string> reachable;
  generate_transition(nodes, neg_vars, reachable, 0, 10);
  return 0;
}
