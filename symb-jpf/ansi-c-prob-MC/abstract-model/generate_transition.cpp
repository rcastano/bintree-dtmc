
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <utility>
#include <vector>

#include "bintree.h"
#include "create_bintree_java.h"
#include "generate_transition.h"

using namespace std;

typedef BinTreeShapeAbstraction BT;
const string RESERVED_NODE_NAME = "NOT_A_NODE";

string declare_symbolic_int(string name) {
  stringstream ss;
  ss << "int " << name << " = Debug.makeSymbolicInteger(\"" << name;
  ss << "\");";
  return ss.str();
}
string binary_tree_add(string name) {
  stringstream ss;
  ss << "b.add(" << name << ");";
  return ss.str();
}


pair<string, int> parse_restriction(istream &in) {
  string line;
  while(getline(in, line)) {
    stringstream temp_stream;
    temp_stream << line;
    string token;
    temp_stream >> token;
    if (token != "constraint") continue;
    int n;
    temp_stream >>  token >> token >> n;
    // constraint   #        =        n
    stringstream result_stream;
    for (int i = 0; i < n; ++i) {
      getline(in, line);
      result_stream << line << " ";
    }
    return make_pair(result_stream.str(), n);
  }
  return make_pair("", 0);
}

pair< pair<string, int>, string > parse_restriction_for_delete(istream &in) {
  pair<string, int> restriction = parse_restriction(in);
  string removed_node = RESERVED_NODE_NAME;
  char temp_char = in.peek();  
  if (in.peek() == 'N') {
    string temp;
    in >> temp >> removed_node;
  }
  return make_pair(restriction, removed_node);
}

// Path condition to barvinok format
string replace_operands(const string &restriction) {
  stringstream in;
  stringstream out;
  in << restriction;
  string token;
  while(in >> token) {
    if (token == "&&") token = "and";
    if (token == "==") token = "=";
    if (token.substr(0, 6) == "CONST_") token = token.substr(6);
    out << token << " ";
  }
  return out.str(); 
}

const string iscc_output_filename_suffix = "temp_iscc.out";

inline string iscc_output_filename(const string &id) {
  return id + iscc_output_filename_suffix;
}

void generate_iscc_query(const BT *r, ostream &iscc_input, const string &output_filename, int min_closed, int max_open ) {
  iscc_input << "P := [] -> { [ e, ";
  r->printInorder(iscc_input, "r", 15);
  iscc_input << "] : ";
  r->printStructuralRestrictions(iscc_input, "r", 15, "and");
  r->printRangeRestrictions(iscc_input, "r", min_closed, max_open, 15, "and");
  iscc_input << " e " << " >= " << min_closed << " and ";
  iscc_input << " e " << " < " << max_open << " }; " << endl;
  iscc_input << "write \"" << output_filename << "\" card P; " << endl;
  
}


void generate_jpf_query_setup(BT *r, const vector<string> &nodes, ostream &java_input, const string &id ) {
	java_input << "BinTree b = new BinTree();" << endl;
  for (vector<string>::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
    java_input << declare_symbolic_int(*it) << endl;
  }
  java_input << declare_symbolic_int("e") << endl;
  java_input << "Debug.assume(";
  r->printStructuralRestrictions(java_input, "r", 15, "&&");
  //r->printRangeRestrictions(java_input, "r", min_closed, max_open, 15, "&&");
  java_input << " true ); " << endl;
  
  for (int i = 0; i < nodes.size(); ++i) {
    java_input << binary_tree_add(nodes[i]) << endl;
  }
  
  // java_input << "b.id = \"" << id << "\";" << endl;
  java_input << "b.id = \"\"; " << endl; 
}

string make_jpf_query_for_add(const stringstream &java_input) {
	return java_input.str() + "\n addElement(Debug.makeSymbolicInteger(\"e\"), b);\n";
}
string make_jpf_query_for_remove(const stringstream &java_input) {
	return java_input.str() + "\n removeElement(Debug.makeSymbolicInteger(\"e\"), b);\n";
}

const string iscc_input_filename_suffix = "temp_iscc.in";

inline string iscc_input_filename(const string &id) {
  return id + iscc_input_filename_suffix;
}

inline string query_id(string phase, string id) {
  return phase + id;
}


// Assumes sorted nodes.
// Outputs a BinTree with all the nodes.
BT *setup_tree(vector<string> nodes) {

  BT *r = new BinTreeShapeAbstraction();
  for (int i = 0 ; i < nodes.size(); ++i) {
    if (nodes[i] != "r") {
      if(!r->add(nodes[i])) return NULL;
    }
  }
  return r;
}

inline string get_id(const string &phase, const string &nodes_id) {
  return phase + nodes_id;
}

void gather_iscc_query(const string &id, const BT *r, int min_closed, int max_open, map<string, string> &queries) {

  stringstream iscc_query_stream;
  generate_iscc_query(r, iscc_query_stream, iscc_output_filename(id), min_closed, max_open);
  if (queries.find(id) != queries.end()) {
    cerr << "Overwriting a query!!" << endl;
  }
  queries[id] = iscc_query_stream.str();
}

inline void execute_iscc_queries(const map<string, string> &queries, const string &phase) {
  const string filename = queries.size() == 1 ? iscc_output_filename(queries.begin()->first): (phase + "grouped_iscc.in");
  {
    ofstream iscc_queries_file(filename.c_str());
    for (map<string, string>::const_iterator it = queries.begin(); it != queries.end(); ++it) {
      iscc_queries_file << it->second << endl;
    }
    iscc_queries_file.flush();
    iscc_queries_file.close();
  }

  system(("cat " + filename + " | ~/lib/bin/iscc").c_str());
}

void gather_iscc_results(const map<string, string> &queries, const string &phase, map<string, string> &model_counts) {
  for (map<string, string>::const_iterator it = queries.begin(); it != queries.end(); ++it) {
    ifstream iscc_output(iscc_output_filename(it->first).c_str());
    if (model_counts.find(it->first) != model_counts.end()) {
      model_counts[it->first] += "+";
    }

    string model_count;
    string temp;
    iscc_output >> temp >> model_count >> temp;
    // Sometimes iscc return { } instead of { 0 }
    if (model_count == "}") model_count = "0";
#ifdef DEBUG
    cout << model_count << endl;
#endif
    model_counts[it->first] += model_count;
  }
}

const string jpf_input_filename_suffix = "temp_jpf.in";

inline string jpf_input_filename(const string &id) {
  return id + jpf_input_filename_suffix;
}


// last parameter is output
void generate_transition(vector<string> nodes, vector<string> &neg_vars, vector<string> &reachable, int min_closed, int max_open) {
  sort(nodes.begin(), nodes.end());
  BT *r = setup_tree(nodes);
  if (r == NULL) return ;
  string nodes_id;
  {
    stringstream nodes_stream;
    for (vector<string>::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
      nodes_stream << *it;
    }
    nodes_id = nodes_stream.str();
  }
  string source_model_count;
  {
    map<string, string> source_query;
    gather_iscc_query(get_id("source", nodes_id), r, min_closed, max_open, source_query);
    execute_iscc_queries(source_query, "source");
    map<string, string> model_count;
    gather_iscc_results(source_query, "source", model_count);
    source_model_count = model_count.begin()->second;
  }
  
  stringstream java_input;
  generate_jpf_query_setup(r, nodes, java_input, get_id("source", nodes_id));
  {
    ofstream java_source("./aux_files/BinTree.java");
    create_bintree_java("aux_files/BinTree.java.tmp", make_jpf_query_for_add(java_input), java_source);   
    system("./aux_files/compile_bintree.sh ");
    system(" jpf ./aux_files/BinTree.jpf > output_jpf.out");
  }

  // I store counts in string because they can be
  // very large numbers.
  map<string, string> model_counts;
  {
    ifstream jpf_output("output_jpf.out");
    string restriction;
    do {

      pair<string, int> restriction_n = parse_restriction(jpf_output);
      restriction = replace_operands(restriction_n.first);
      int n = restriction_n.second;

      stringstream ss;
      ss << restriction_n.first;
      if (n==0) continue;
#ifdef DEBUG
      cerr << "add" << endl;
      cerr << restriction << endl;
#endif
      string node = BT::restriction_to_prefix(ss, n);
#ifdef DEBUG
      cout << "node: " << node << endl << endl;
#endif
      { 
        ofstream iscc_input((node + "temp_iscc.in").c_str());

        iscc_input << "P := [] -> { [ e, ";
        r->printInorder(iscc_input, "r", 15);
        iscc_input << "] : ";
        r->printRangeRestrictions(iscc_input, "r", min_closed, max_open, 15, "and");
        iscc_input << restriction;
        iscc_input << " and e " << " >= " << min_closed << " and ";
        iscc_input << " e " << " < " << max_open << " }; " << endl;
        iscc_input << "card P; " << endl;

        system(("cat " + node + "temp_iscc.in | ~/lib/bin/iscc > " + node + "temp_iscc.out").c_str());
        
        ifstream iscc_output((node + "temp_iscc.out").c_str());
        string destination_model_count;
        string temp;
        iscc_output >> temp >> destination_model_count >> temp;
        // Sometimes iscc return { } instead of { 0 }
        if (destination_model_count == "}") destination_model_count = "0";
#ifdef DEBUG
        cout << destination_model_count << endl;
#endif
        model_counts[node] = destination_model_count;
      }
    } while(restriction != "" );
  }
  ofstream prism_script("generated.prism");
  prism_script << "[step] !bug & !other & ";
  for (int i = 0; i < nodes.size(); ++i) {
    if (i != 0) prism_script << " & ";
    prism_script << nodes[i];
  }
  for (int i = 0; i< neg_vars.size(); ++i) {
    prism_script << " & !" << neg_vars[i];
  }
  prism_script << " -> ";
  {
    map<string, string>::const_iterator it = model_counts.begin();
    bool add_plus = false;
    for ( ; it != model_counts.end(); ++it) {
      reachable.push_back(it->first);
      if (add_plus) prism_script << " + ";
      add_plus = true;
      prism_script << "(PA*(" << it->second << "/" << source_model_count << ")):(";
      if (find(neg_vars.begin(), neg_vars.end(), it->first) == neg_vars.end() &&
          find(nodes.begin(), nodes.end(), it->first) == nodes.end()) {
        prism_script << "other'=true)";
        for (int i = 0; i < nodes.size(); ++i) {
          prism_script << "&(";
          prism_script << nodes[i] << "'=false)";
        }
        for (int i = 0; i< neg_vars.size(); ++i) {
          prism_script << "&(";
          prism_script << neg_vars[i] << "'=false)";
        }
      } else {
        prism_script << it->first << "'=true)";
      }
    }
    // TODO finish the transition, missing the delete part.
    // prism_script << ";" << endl;
  }

  
  {
    ofstream java_source("./aux_files/BinTree.java");
    create_bintree_java("aux_files/BinTreeDeletionMCBug.java.tmp", java_input.str(), java_source);   
    system("./aux_files/compile_bintree.sh ");
    system(" jpf ./aux_files/BinTree.jpf > output_jpf.out");
  }
  // I store counts in string because they can be
  // very large numbers.
  map<string, string> model_counts_remove;
  {
    ifstream jpf_output("output_jpf.out");
    string restriction;
    do {

      pair<string, int> restriction_n = parse_restriction(jpf_output);
      restriction = replace_operands(restriction_n.first);
      int n = restriction_n.second;

      stringstream ss;
      ss << restriction_n.first;
      if (n==0) continue;
#ifdef DEBUG
      cerr << "remove Bug" << endl;
      cerr << restriction << endl;
#endif
      string node = BT::restriction_to_prefix(ss, n);
#ifdef DEBUG
      cout << "node: " << node << endl << endl;
#endif
      { 
        ofstream iscc_input((node + "temp_iscc.in").c_str());

        iscc_input << "P := [] -> { [ e, ";
        r->printInorder(iscc_input, "r", 15);
        iscc_input << "] : ";
        r->printRangeRestrictions(iscc_input, "r", min_closed, max_open, 15, "and");
        iscc_input << restriction;
        iscc_input << " and e " << " >= " << min_closed << " and ";
        iscc_input << " e " << " < " << max_open << " }; " << endl;
        iscc_input << "card P; " << endl;

        system(("cat " + node + "temp_iscc.in | ~/lib/bin/iscc > " + node + "temp_iscc.out").c_str());
        
        ifstream iscc_output((node + "temp_iscc.out").c_str());
        string destination_model_count;
        string temp;
        iscc_output >> temp >> destination_model_count >> temp;
        if (destination_model_count == "}") destination_model_count = "0";
#ifdef DEBUG
        cout << destination_model_count << endl;
#endif
        model_counts_remove[node] = destination_model_count;
      }
    } while(restriction != "" );
  }
  {
    map<string, string>::const_iterator it = model_counts_remove.begin();
    prism_script << " + (PD*(0";
    for ( ; it != model_counts_remove.end(); ++it) {
      prism_script << "+";
      prism_script << it->second;
    }
    prism_script << ")/" << source_model_count << "):(bug'=true)";
    for (int i = 0; i < nodes.size(); ++i) {
      prism_script << "&(";
      prism_script << nodes[i] << "'=false)";
    }
    for (int i = 0; i< neg_vars.size(); ++i) {
      prism_script << "&(";
      prism_script << neg_vars[i] << "'=false)";
    }
  }
















  {
    ofstream java_source("./aux_files/BinTree.java");
    create_bintree_java("aux_files/BinTreeDeletionOK.java.tmp", java_input.str(), java_source);   
    system("./aux_files/compile_bintree.sh ");
    system(" jpf ./aux_files/BinTree.jpf > output_jpf.out");
  }
  // I store counts in string because they can be
  // very large numbers.
  model_counts_remove.clear();
  {
    ifstream jpf_output("output_jpf.out");
    string restriction;
    do {
      pair< pair<string, int>, string> restriction_n_removed = parse_restriction_for_delete(jpf_output);
      pair<string, int> restriction_n = restriction_n_removed.first;
      restriction = replace_operands(restriction_n.first);
      int n = restriction_n.second;

      stringstream ss;
      ss << restriction_n.first;
      if (n==0) continue;
#ifdef DEBUG
      cerr << "remove ok" << endl;
      cerr << restriction << endl;
#endif
      string node = restriction_n_removed.second;
#ifdef DEBUG
       cout << "node: " << node << endl << endl;
#endif
      { 
        ofstream iscc_input((node + "temp_iscc.in").c_str());

        iscc_input << "P := [] -> { [ e, ";
        r->printInorder(iscc_input, "r", 15);
        iscc_input << "] : ";
        r->printRangeRestrictions(iscc_input, "r", min_closed, max_open, 15, "and");
        iscc_input << restriction;
        iscc_input << " and e " << " >= " << min_closed << " and ";
        iscc_input << " e " << " < " << max_open << " }; " << endl;
        iscc_input << "card P; " << endl;

        system(("cat " + node + "temp_iscc.in | ~/lib/bin/iscc > " + node + "temp_iscc.out").c_str());
        
        ifstream iscc_output((node + "temp_iscc.out").c_str());
        string destination_model_count;
        string temp;
        iscc_output >> temp >> destination_model_count >> temp;
        if (destination_model_count == "}") destination_model_count = "0";
#ifdef DEBUG
        cout << destination_model_count << endl;
#endif
        // TODO check whether it's right to add the probabilities
        // Detailed explanation: suppose there are two path conditions
        // that lead to the removal of a particular node.
        // Are we supposed to add the number of models for both or not?
        // I guess this can be stated more generally as a question about
        // JPF: Are the PCs disjoint? 
        if (model_counts_remove.find(node) != model_counts_remove.end()) {
          model_counts_remove[node] += "+";
        }
        model_counts_remove[node] += destination_model_count;
      }
    } while(restriction != "" );
  }

  {
    map<string, string>::const_iterator it = model_counts_remove.begin();
    prism_script << " + (PD*(0";
    for ( ; it != model_counts_remove.end(); ++it) {
      // This loop handles the case where nothing changed.
      if (find(nodes.begin(), nodes.end(), it->first) != nodes.end()) continue;
      prism_script << "+";
      prism_script << it->second;
    }
    prism_script << ")/" << source_model_count << "):(other'=other)";

// This code shouldn't be here, this is a transition where
// a non existing node was deleted. Why set everything else
// as deleted?
//    for (int i = 0; i < nodes.size(); ++i) {
//      prism_script << "&(";
//      prism_script << nodes[i] << "'=false)";
//    }
//    for (int i = 0; i< neg_vars.size(); ++i) {
//      prism_script << "&(";
//      prism_script << neg_vars[i] << "'=false)";
//    }



    it = model_counts_remove.begin();
    for ( ; it != model_counts_remove.end(); ++it) {
      // This loop handles the case where a node
      // was actually deleted.
      if (find(nodes.begin(), nodes.end(), it->first) == nodes.end()) continue;
      prism_script << " + (PD*((";
      prism_script << it->second << ")/" << source_model_count << ")):(" << it->first << "'=false)";
    }


  }
  prism_script << ";" << endl;
}
