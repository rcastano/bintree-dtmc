#include <iostream>
#include <fstream>

#include "create_bintree_java.h"

using namespace std;

int create_bintree_java(const string &input_java, const string &replacement, ostream &out) {
  ifstream java_template(input_java.c_str());
  string temp;
  while (getline(java_template, temp)) {
    if (temp == "REPLACE_THIS_LINE") {
      out << replacement << endl;
    } else {
      out << temp << endl;
    }
  } 
  
  return 0;
}
