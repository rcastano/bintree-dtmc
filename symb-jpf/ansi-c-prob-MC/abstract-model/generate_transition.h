#ifndef GENERATE_TRANSITION_H
#define GENERATE_TRANSITION_H

#include <string>
#include <vector>


// These are supposed to be sorted in an order
// in which it's possible to add them to a tree.
// For example:
// r, r0, r1, r01
void generate_transition(std::vector<std::string> nodes, std::vector<std::string> &neg_vars, std::vector<std::string> &reachable, int min_closed, int max_open);

#endif // GENERATE_TRANSITION_H
