#include <iostream>
#include <sstream>


#include "bintree.h"

typedef BinTreeShapeAbstraction BT;
int main() {
//  BT r, r0, r1, r00, r01;
//  
//  r.left = &r0;
//  r.right = &r1;
//  r0.left = &r00;
//  r0.right = &r01;
//  r.printInorder(std::cout, "r");
  BT *r = BT::make_full_tree(1); 

  r->add("r1");

  r->printInorder(std::cout, "r", 15);


  std::cout << std::endl << std::endl;
  
  r->printStructuralRestrictions(std::cout, "r", 15, "and");
  r->printRangeRestrictions(std::cout, "r", 1, 10, 15, "and");

  std::cout << std::endl << std::endl;
  std::stringstream ss;
  ss <<"r > e and "
      "r1 > r and "
      "r1 == r1 and "
      "r <= r1 and "
      "r != r1";
  std::cout << BT::restriction_to_prefix(ss, 5) << std::endl;
  return 0;
}
