#ifndef BINTREE_H
#define BINTREE_H
#include <iostream>
#include <string>
#include <vector>

struct BinTreeShapeAbstraction {
  BinTreeShapeAbstraction *left;
  BinTreeShapeAbstraction *right;
  BinTreeShapeAbstraction();
  ~BinTreeShapeAbstraction();
  // TODO this has memory leaks, fix.
  void printStructuralRestrictions(std::ostream &os, const std::string &prefix, int max_prefix_length, std::string conjunction) const;
  void printRangeRestrictions(std::ostream &os, const std::string &prefix, int min_closed, int max_open, int max_prefix_length, std::string conjunction) const;
  void printNodeNames(std::ostream &os, const std::string &prefix, int max_prefix_length) const;
  void printInorder(std::ostream &os, const std::string &prefix, int max_prefix_length, bool print_comma = false) const;
  void inorder(std::vector<std::string> &result, const std::string &prefix, int max_prefix_length) const;
  static BinTreeShapeAbstraction *make_full_tree(int height);
  BinTreeShapeAbstraction **traverse(std::string prefix);
  bool add(std::string prefix);
  void remove(std::string prefix);
  int height() const;
  int number_of_nodes() const;
  static std::string restriction_to_prefix(std::istream &restriction, int t);

  static bool sanity_check(std::vector<std::string> nodes, int min_closed, int max_open);
};



#endif  // BINTREE_H
