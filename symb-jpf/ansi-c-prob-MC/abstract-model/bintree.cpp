
#include <algorithm>
#include <cstdlib>
#include <deque>
#include <map>
#include <queue>
#include <sstream>
#include <utility>
#include <vector>

#include "bintree.h"
using namespace std;
BinTreeShapeAbstraction::BinTreeShapeAbstraction() : left(NULL), right(NULL) {}
BinTreeShapeAbstraction::~BinTreeShapeAbstraction() {
//  deque<BinTreeShapeAbstraction *> q;
//  q.push_back(this->left);
//  q.push_back(this->right);
//  while (!q.empty()) {
//    BinTreeShapeAbstraction * current = q.front();
//    q.pop_front();
//    if (current == NULL) continue;
//    q.push_back(current->left);
//    q.push_back(current->right);
//    delete current;
//  }
}
void BinTreeShapeAbstraction::printStructuralRestrictions(std::ostream &os, const string &prefix, int max_prefix_length, string conjunction) const {
  conjunction = " " + conjunction + " ";
  queue< pair<BinTreeShapeAbstraction *, string> > greater, smaller;
  smaller.push( make_pair(left, prefix + "0") );
  greater.push( make_pair(right, prefix + "1") );
  queue< pair<BinTreeShapeAbstraction *, string> > *q;
  int i;
  for (q = &smaller, i = 0; i < 2; ++i, q = &greater) {
    while(!q->empty()) {
      BinTreeShapeAbstraction * current = q->front().first;
      string current_prefix = q->front().second;
      q->pop();
      if (current == NULL) continue;
      if (current_prefix.length() > max_prefix_length) continue;
      q->push( make_pair(current->left, current_prefix + "0") );
      q->push( make_pair(current->right, current_prefix + "1") );
      os << prefix << (i==0 ? " > " : " < ") << current_prefix << conjunction << endl;
    }
  }
  if (left != NULL) {
    left->printStructuralRestrictions(os, prefix + "0", max_prefix_length, conjunction);
  }
  if (right != NULL) {
    right->printStructuralRestrictions(os, prefix + "1", max_prefix_length, conjunction);
  }
}

void BinTreeShapeAbstraction::printRangeRestrictions(std::ostream &os, const std::string &prefix, int min_closed, int max_open, int max_prefix_length, string conjunction) const {
  conjunction = " " + conjunction + " ";
  queue< pair<const BinTreeShapeAbstraction *, string> > q;
  q.push( make_pair(this, prefix));
  while(!q.empty()) {
    const BinTreeShapeAbstraction * current = q.front().first;
    string current_prefix = q.front().second;
    q.pop();
    if (current == NULL) continue;
    if (current_prefix.length() > max_prefix_length) continue;
    q.push( make_pair(current->left, current_prefix + "0") );
    q.push( make_pair(current->right, current_prefix + "1") );
    os << current_prefix << " >= " << min_closed << conjunction << current_prefix << " < " << max_open << conjunction << endl;
  }
}

void BinTreeShapeAbstraction::printNodeNames(std::ostream &os, const std::string &prefix, int max_prefix_length) const {
  queue< pair<const BinTreeShapeAbstraction *, string> > q;
  q.push( make_pair(this, prefix));
  while(!q.empty()) {
    const BinTreeShapeAbstraction * current = q.front().first;
    string current_prefix = q.front().second;
    q.pop();
    if (current == NULL) continue;
    if (current_prefix.length() > max_prefix_length) continue;
    q.push( make_pair(current->left, current_prefix + "0") );
    q.push( make_pair(current->right, current_prefix + "1") );
    os << current_prefix << ", ";
  }
}


void BinTreeShapeAbstraction::inorder(vector<string> &result, const std::string &prefix, int max_prefix_length) const {
  if (prefix.length() > max_prefix_length) return;
  if (left != NULL) left->inorder(result, prefix + "0", max_prefix_length);
  result.push_back(prefix);
  if (right != NULL) right->inorder(result, prefix + "1", max_prefix_length);

}
void BinTreeShapeAbstraction::printInorder(std::ostream &os, const std::string &prefix, int max_prefix_length, bool print_comma) const {
  vector<string> nodes;
  inorder(nodes, prefix, max_prefix_length);
  for (int i = 0; i < nodes.size(); ++i) {
    if (i!= 0) os << ", ";
    os << nodes[i];
  }
}
BinTreeShapeAbstraction *BinTreeShapeAbstraction::make_full_tree(int height) {
  BinTreeShapeAbstraction * tree_root = NULL;
  if (height != 0) {
    tree_root = new BinTreeShapeAbstraction();
    tree_root->left = make_full_tree(height - 1);
    tree_root->right = make_full_tree(height - 1);
  }
  return tree_root;
}

void abort(string msg) {
  cerr << "Aborting: " << msg << endl;
  exit(1);
}
BinTreeShapeAbstraction **BinTreeShapeAbstraction::traverse(string prefix) {
  int i = 2;
  if(prefix == "r") {
    abort("Can't traverse to the root, already there...");
  }
  BinTreeShapeAbstraction **node;
  if (prefix[1] == '1') {
    node = &this->right;
  } else {
    node = &this->left;
  }

  while(i < prefix.size()) {
    if (*node == NULL) {
      return NULL;
      abort("Error traversing the tree.");
    }
    if (prefix[i] == '1') {
      node = &(*node)->right;
    } else {
      node = &(*node)->left;
    }
    ++i;
  }
  return node;
}
bool BinTreeShapeAbstraction::add(std::string prefix) {
  BinTreeShapeAbstraction** leaf = traverse(prefix);
  if (leaf == NULL) return false;
  if (*leaf != NULL) {
    return false;
    abort("Didn't reach a suitable leaf.");
  }
  *leaf = new BinTreeShapeAbstraction();
  return true;
}
void BinTreeShapeAbstraction::remove(std::string prefix) {
  BinTreeShapeAbstraction** leaf = traverse(prefix);
  delete *leaf;
  *leaf = NULL;
}

bool is_comparator_lookahead(char temp) {
  return temp == '&' ||
         temp == '<' ||
         temp == '>' ||
         temp == '!' ||
         temp == '=';
} 
struct Restriction {
  string local;
  // This is almost useless as an operator parser, 
  // but it turns out we'll only care about the
  // restrictions that involve "e" and a node "r010...1", 
  // which are the ones that correspond to the traversal
  // that took place.
  string parse_operator(istream &in) {
    stringstream op_s;
    char temp;
    while (in.get(temp) && !is_comparator_lookahead(temp)){
      if (temp != ' ') op_s.put(temp);
    }
    if (is_comparator_lookahead(temp)) in.unget();
    return  op_s.str();

  }
  bool make_restriction(istream &in) {
    string op1, op2, comp_string;
    op1 = parse_operator(in);
    in >> comp_string;
    op2 = parse_operator(in);
    if (op1[0] == 'e') {
      if (op2[0] != 'r') {
        // cerr << "No local operand: " << op1 << " " << comp_string << " " << op2 << endl;
        return false;
        // abort("No local operand");
      }
      local = op2;
      comp = string_to_COMP(comp_string, false);
    } else if(op2[0] == 'e') {
      if (op1[0] != 'r') {
        // cerr << "No local operand: " << op1 << " " << comp_string << " " << op2 << endl;
        // abort("No local operand");
        return false;
      }
      local = op1;
      comp = string_to_COMP(comp_string, true);
    } else {
      // cerr << "No 'e' operand: " << op1 << " " << comp_string << " " << op2 << endl;
      return false;
    }
    return true;
  }
  enum COMP {LT, GT, LTEQ, GTEQ, EQ, NEQ};
  COMP comp;
  COMP string_to_COMP(string s, bool invert_ops) {
    if (s == "<") {
      return (invert_ops ? GT : LT);  
    } else if (s == ">") {
      return (invert_ops ? LT : GT); 
    } else if (s == "<=") {
      return (invert_ops ? GTEQ : LTEQ);
    } else if (s == ">=") {
      return (invert_ops ? LTEQ : GTEQ);
    } else if (s == "==") {
      return EQ;
    } else if (s == "=") {
      return EQ;
    } else if (s == "!=") {
      return NEQ;
    } else {
      abort("Unknown comparator: " + s);
    }
  }
  static void join_comparators(vector<COMP> &comps) {
    COMP comp = comps[0];
    for (int i = 1; i < comps.size(); ++i) {
      comp = join(comp, comps[i]);
    }
    comps.clear();
    comps.push_back(comp);
  }
  static COMP join(const COMP &a, const COMP &b) {
    if (a == LT) {
      if (b == GT || b == EQ || b == GTEQ) {
        abort("Unsatisfiable restrictions");
      }
      return LT;
    }
    if (a == GT) {
      if (b == LT || b == EQ || b == LTEQ ) {
        abort("Unsatisfiable restrictions");
      }
      return GT;
    }
    if (a == LTEQ) {
      if (b == GT) {
        abort("Unsatisfiable restrictions");
      }
      if (b == LT) return LT;
      if (b == NEQ) return LT;
      if (b == LTEQ) return LTEQ;
      return EQ; 
    }
    if (a == GTEQ) {
      if (b == LT) {
        abort("Unsatisfiable restrictions");
      }
      if (b == GT) return GT;
      if (b == NEQ) return GT;
      if (b == GTEQ) return GTEQ;
      return EQ;
    }
    if (a == EQ) {
      if (b == LT || b == GT || b == NEQ) {
        abort("Unsatisfiable restrictions");
      }
      return EQ;
    }
    if (a == NEQ) {
      if (b == EQ) {
        abort("Unsatisfiable restrictions");
      }
      if (b == LTEQ) {
        return LT;
      }
      if (b == GTEQ) {
        return GT;
      }
      return b;
    }
    abort("Unknown comparator in join");
  }
  bool operator<(const Restriction &other) const {
    return local < other.local || ( local == other.local && comp < other.comp);
  }
};


string BinTreeShapeAbstraction::restriction_to_prefix(istream &ss, int t) {
  string temp;
  // map nodes to restrictions
  map<string, vector<Restriction::COMP> > res;
  for (int i= 0; i < t; ++i) {
    Restriction r;
    bool discard = !r.make_restriction(ss);
    // Need to read the conjunction operator
    if (i + 1 < t) ss >> temp;
    if (discard) continue;
    res[r.local].push_back(r.comp);
    Restriction::join_comparators(res[r.local]); 

  }
  stringstream s_prefix;
  s_prefix << "r";
  while(!res[s_prefix.str()].empty()) {
    // cerr << "prefix: " << s_prefix.str() << endl;
    if (res[s_prefix.str()].front() == Restriction::EQ) {
      break;
    }
    if (res[s_prefix.str()].front() == Restriction::LT) {
      s_prefix << "0";
    } else if (res[s_prefix.str()].front() == Restriction::GT) {
      s_prefix << "1";
    }
  }
  return s_prefix.str();
}

int BinTreeShapeAbstraction::height() const {

  int a = 0;
  int b = 0;
  if (left != NULL) a = left->height();
  if (right != NULL) b = right->height();
  if (b > a) a = b;
  return a + 1;

}
int BinTreeShapeAbstraction::number_of_nodes() const {
  vector<string> nodes;
  inorder(nodes, "r", height()+2);
  return nodes.size();
}

bool BinTreeShapeAbstraction::sanity_check(std::vector<std::string> nodes, int min_closed, int max_open) {
  cerr << "sanity_check" << endl;
  if (nodes.empty()) return false;
  sort(nodes.begin(), nodes.end());
  if (nodes[0] != "r") return false;

  BinTreeShapeAbstraction t;
  for (int i = 1; i < nodes.size(); ++i) {
    cerr << nodes[i] << " "; 
    if (nodes[i] == "r") continue;
    BinTreeShapeAbstraction **node = t.traverse(nodes[i]);
    if (node == NULL) return false;
    *node = new BinTreeShapeAbstraction();
  }
  if (t.number_of_nodes() > max_open - min_closed) return false;
  cerr << endl;
  return true;
}
