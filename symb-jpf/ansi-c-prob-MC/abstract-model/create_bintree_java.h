#ifndef CREATE_BINTREE_JAVA_H_
#define CREATE_BINTREE_JAVA_H_

#include <iostream>
#include <string>

int create_bintree_java(const std::string &input_java, const std::string &replacement, std::ostream &out);

#endif  // CREATE_BINTREE_JAVA_H_

