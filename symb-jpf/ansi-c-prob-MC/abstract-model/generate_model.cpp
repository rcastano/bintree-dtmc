#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

#include "bintree.h"
#include "generate_transition.h"

using namespace std;

typedef BinTreeShapeAbstraction BT;
const int min_closed = 0;
const int max_open = 10;

int main() {
  BT *full = BT::make_full_tree(3);
  vector<string> nodes;

  system(" rm whole_model.prism");
  system(" touch whole_model.prism");
  full->inorder(nodes, "r", 15);

  ofstream whole_model("whole_model.prism");
  whole_model << "dtmc" << endl << endl;
  whole_model << "const double PA = 0.5;" << endl << endl;
  whole_model << "const double PD = 0.5;" << endl << endl;
  whole_model << "module bintree " << endl;
  for (int i = 0; i < nodes.size(); ++i) {
    whole_model << nodes[i] << ":bool init false; " << endl;
  }
  whole_model << "bug:bool init false; " << endl;
  whole_model << "other:bool init false; " << endl;

  whole_model << "[step] bug & !other ";
  for (int i = 0; i < nodes.size(); ++i) {
    whole_model << " & !" << nodes[i] << " ";
  } 
  whole_model << " -> (bug'=true);" << endl;
  whole_model << "[step] !bug & other ";
  for (int i = 0; i < nodes.size(); ++i) {
    whole_model << " & !" << nodes[i] << " ";
  } 
  whole_model << " -> (other'=true);" << endl;


  whole_model << "[step] !bug & !other ";
  for (int i = 0; i < nodes.size(); ++i) {
    whole_model << " & !" << nodes[i] << " ";
  } 
  whole_model << " -> PA:(r'=true) + PD:(other'=other);" << endl;

  whole_model.close();
  vector<bool> selected(nodes.size(), false);
  while(find(selected.begin(), selected.end(), false) != selected.end()) {
    for (int i = 0; i < selected.size(); ++i) {
      selected[i] = !selected[i];
      if (selected[i]) break;
    }
    vector<string> actual_nodes, reachable, neg_vars;
    for (int i = 0; i < selected.size(); ++i) {
      if (selected[i]) {
        actual_nodes.push_back(nodes[i]);
      } else {
        neg_vars.push_back(nodes[i]);
      }
    }
    if (!BT::sanity_check(actual_nodes, min_closed, max_open)) continue;
    generate_transition(actual_nodes, neg_vars, reachable, min_closed, max_open);
    system(" echo     >> whole_model.prism");
    system(" echo     >> whole_model.prism");
    system(" echo     >> whole_model.prism");
    system(" echo     >> whole_model.prism");
    system(" cat generated.prism >> whole_model.prism");

  }

    system(" echo endmodule    >> whole_model.prism");
    system(" echo rewards    >> whole_model.prism");
    system(" echo [step] true : 1\\;    >> whole_model.prism");
    system(" echo endrewards    >> whole_model.prism");
}



